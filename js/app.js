var app = angular.module('galleryApp', ['ngRoute', 'ngSanitize', 'mgcrea.ngStrap', 'ngMessages', 'ngMaterial', 'md-steppers', 'ngCookies', 'ui.router', 'ui.toggle', 'vcRecaptcha', 'oc.lazyLoad', 'checklist-model', 'wu.masonry', 'useragent', 'textAngular']);

var REQUEST_STORAGE_KEY = 'REQ';
var FILTER_STORAGE_KEY = 'FILTER';
var CATE_SEARCH_STORAGE_KEY = 'CATE';
var ALLPAGE_STORAGE_KEY = 'ALLP';
var ARTPAGE_STORAGE_KEY = 'ARTP';
var ARCPAGE_STORAGE_KEY = 'ARCP';
var LIBPAGE_STORAGE_KEY = 'LIBP';
var ADVSEARCHCATE_STORAGE_KEY = 'ADVSVC';
var JSON_ADVSEARCH_STORAGE_KEY = 'ADVS';
var JSON_BSSEARCH_STORAGE_KEY = 'BSS';
var JSON_FILTERSEARCH_STORAGE_KEY = 'FLS';
var JSON_ISCHECKTERM_STORAGE_KEY = 'TERM';
var RELATEDITEM_STORAGE_KEY = 'RITEM';
var ITEM2DETAILS_STORAGE_KEY = 'ITEMDETAILS';
var DOWNTIME = false;
var IMAGE_PATH = 'https://production-ngssource.s3-ap-southeast-1.amazonaws.com/'

app.config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // debug: true,
        events: true
    });
}]);

app.config(function () {
    angular.lowercase = angular.$$lowercase;
});
// config(function($addthisProvider) {
//     $addthisProvider.profileId('ra-5d3828c35db51846');
// })


app.controller('mainCtrl', ['$scope', 'ApiService', '$state', '$location', '$timeout', '$window', '$rootScope', function ($scope, ApiService, $state, $location, $timeout, $window, $rootScope) {
    $rootScope.imagePath = IMAGE_PATH;
    $rootScope.term = sessionStorage
    sessionStorage[JSON_ISCHECKTERM_STORAGE_KEY] = 'false';
    $scope.requestLength = sessionStorage[REQUEST_STORAGE_KEY] ? JSON.parse(sessionStorage[REQUEST_STORAGE_KEY]).length : 0;
    $scope.location = '';
    $scope.hasOptionBar = false;
    $scope.switchPlaceholder = 'Search across Artwork, Archive & Library items...';

    if(DOWNTIME){
        $state.go('downtime');
    }

    $rootScope.homeTop = true;
    $scope.home = function () {
        if ($scope.location == 'home') {
            $('html, body').animate({
                scrollTop: 0
            }, 0);
            return false;
        } else {
            $state.go('home');
        }
    }

    $scope.cart = function () {
        $state.go('cart');
    }

    // ApiService.checkmode().then(function(r) {
    //     $scope.siteSwitch = r.data.Data.IsOnSite;
    // });


    // $scope.siteSwitchFunc = function(val) {
    //     $scope.cur_mode = {
    //         IsOnSite: val
    //     };
    //     ApiService.SetMode($scope.cur_mode).then(function(r) {
    //         if (r.data.Status === 'OK') {
    //             $window.location.reload();
    //         }
    //     });
    // }

    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll > 500) {
            $('a.magnifying').css({
                'display': 'block'
            });

        } else {
            $('a.magnifying').css({
                'display': 'none'
            });
            $('#searchNav').removeClass('active');
            angular.element('#collectionOptionBar').removeClass('active');
        }
    });

    $scope.toggleOptionsBar = function () {
        if ($scope.location == 'home') {
            angular.element('#collectionOptionBar').toggleClass('active');
        } else if ($scope.location == 'search') {
            angular.element('#searchNav').toggleClass('active');
        }
    };

    $scope.toggleNavBar = function () {
        $('.mobile-navigation').toggleClass('active');
    }

    $rootScope.$on('$viewContentLoaded', function (event) {
        // $window.ga('send', 'pageview', $location.path());
    });
}]);

app.controller('homeCtrl', ['$scope', '$rootScope', '$state', '$location', '$http', 'ApiService', 'anchorSmoothScroll', '$window', '$timeout', '$compile', 'useragentProperties', function ($scope, $rootScope, $state, $location, $http, ApiService, anchorSmoothScroll, $window, $timeout, $compile, useragentProperties) {
    $scope.device = useragentProperties.touch;
    $scope.$parent.location = 'home';
    $scope.$parent.hasOptionBar = true;
    $scope.isMoreResult = true;
    $scope.pageNumber = 1;
    $scope.pageSize = 15;
    $scope.loadPage = true;
    $scope.allTabFirstTimeLoad = true;
    $scope.allTabCategory = "Lib";
    $scope.categoryOption = 'ALL';
    if ($scope.device) {
        $scope.masonry = {
            //itemSelector: '.masonry-block',
            //horizontalOrder: false,
            //columnWidth: '.masonry-block',
            gutter: '.gutter-sizer',
            //initLayout: false,
            //animate: true,
            //percentPosition: true,
            transitionDuration: '0s',
        }
    } else {
        $scope.masonry = {
            //itemSelector: '.masonry-block',
            //horizontalOrder: false,
            //columnWidth: '.masonry-block',
            gutter: '.gutter-sizer',
            //initLayout: false,
            //animate: true,
            //percentPosition: true,
            transitionDuration: '0s',
        }
    }

    if(DOWNTIME){
        $state.go('downtime');
    }

    var $grid = angular.element('.grid');
    var $allGrid = angular.element('.allGrid');
    var $artGrid = angular.element('.artGrid');
    var $libGrid = angular.element('.libGrid');
    var $arcGrid = angular.element('.arcGrid');

    $scope.genericSearch = {
        Search: ''
    }

    $scope.AdvanceSearch = {
        CategoryCode: $scope.activeCategory,
        PageNumber: $scope.pageNumber,
        PageSize: $scope.pageSize,
        BasicSearch: false
    }
    ApiService.GetProductCount($scope.AdvanceSearch).then(function (r) {
        $scope.productsLength = r.data.TotalCount;
        $scope.artLength = r.data.CountART;
        $scope.arcLength = r.data.CountARC;
        $scope.libLength = r.data.CountLIB;
    });

    $scope.init = function () {
        $scope.allGridData = [];
        $scope.artGridData = [];
        $scope.arcGridData = [];
        $scope.libGridData = [];

        $scope.imagesPreload = true;

        $scope.isMoreResultForArt = true;
        $scope.isMoreResultForAll = true;
        $scope.isMoreResultForArc = true;
        $scope.isMoreResultForLib = true;

        sessionStorage[ALLPAGE_STORAGE_KEY] = 1;
        sessionStorage[ARTPAGE_STORAGE_KEY] = 1;
        sessionStorage[ARCPAGE_STORAGE_KEY] = 1;
        sessionStorage[LIBPAGE_STORAGE_KEY] = 1;
        $scope.loading = true;
        $scope.pageNumber = 1;
        $scope.loadPage = true;
        //getProduct($scope.AdvanceSearch);
        $scope.loadMore();
    }

    $(document).ready(function () {

        var controller = new ScrollMagic.Controller({
            triggerHook: 'onLeave'
        });
        $(function () {
            controller.enabled(true);

            new ScrollMagic.Scene({
                triggerElement: "#search-section",
                duration: '100%'
            }).on("enter", function (event) {

                $scope.$apply(function () {
                    $scope.$parent.homeTop = false;
                });
            })
                .setClassToggle("#nav-search", "active")
                .addTo(controller);

            new ScrollMagic.Scene({
                triggerElement: ".collection-section",
                triggerHook: 0,
                duration: 0
            }).on("enter", function (event) {
                $scope.$apply(function () {
                    $scope.$parent.homeTop = true;

                });
            })
                .setClassToggle("#nav-collection", "active")
                .addTo(controller);
        });


        $scope.AdvanceSearch = {
            Search: null
        }
        $scope.basicSearch = function (val) {
            sessionStorage[FILTER_STORAGE_KEY] = 'b';
            var search_value = $scope.genericSearch.Search;
            sessionStorage[JSON_BSSEARCH_STORAGE_KEY] = search_value;
            $state.go("search", {
                'Search': search_value,
                'SearchType': 'b',
                'det': null
                // 'CategoryCode': $scope.toggleValue ? $scope.toggleValue : ''
            });
        }

        $scope.focusedIndex = 0;
        $('#homepage').on('keydown', function (event) {

            var keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode === 13) {
                event.preventDefault();
                if (!angular.element(".search-dropdown").hasClass("avtive")) {
                    sessionStorage[FILTER_STORAGE_KEY] = 'b';
                    var search_value = $scope.genericSearch.Search;
                    sessionStorage[JSON_BSSEARCH_STORAGE_KEY] = search_value;
                    $state.go("search", {
                        'Search': search_value,
                        'SearchType': 'b',
                        'det': null
                        // 'CategoryCode': $scope.toggleValue ? $scope.toggleValue : ''
                    });
                } else {
                    $scope.AdvSearch();
                }

            }

        });

    });

    $scope.handleKeyDown = function ($event) {
        var keyCode = $event.keyCode;

        if (keyCode === 40) {
            // Down
            $event.preventDefault();
            if ($scope.focusedIndex !== $scope.autoCompleteLength - 1) {
                $scope.focusedIndex++;
                $scope.genericSearch.Search = angular.element('.autocompleteResults li').eq($scope.focusedIndex).text();

            }
        } else if (keyCode === 38) {
            // Up
            $event.preventDefault();
            if ($scope.focusedIndex !== 0) {
                $scope.focusedIndex--;
                $scope.genericSearch.Search = angular.element('.autocompleteResults li').eq($scope.focusedIndex).text();

            }
        }

    };

    $scope.loadMore = function () {

        if ($scope.loadPage) {
            if ($scope.toggleValue == 'ALL' && $scope.isMoreResultForAll) {

                $scope.loading = true;
                $scope.pageNumber = parseInt(sessionStorage[ALLPAGE_STORAGE_KEY]);

                // var cur_val = $scope.pageNumber;
                // var new_val = cur_val + 1;
                $scope.AdvanceSearch = {
                    CategoryCode: $scope.activeCategory,
                    PageNumber: $scope.pageNumber,
                    PageSize: $scope.pageSize,
                    BasicSearch: false
                }

                if ($scope.allTabCategory == "Art") {
                    $scope.allTabCategory = "Arc";
                } else if ($scope.allTabCategory == "Arc") {
                    $scope.allTabCategory = "Lib";
                } else if ($scope.allTabCategory == "Lib") {
                    $scope.allTabCategory = "Art";
                }

                getProduct($scope.AdvanceSearch);
                if ($scope.allTabCategory == "Lib" && !$scope.allTabFirstTimeLoad) {
                    $scope.pageNumber = $scope.pageNumber + 1;
                }

                sessionStorage[ALLPAGE_STORAGE_KEY] = $scope.pageNumber;

            } else if ($scope.toggleValue == 'ART' && $scope.isMoreResultForArt) {

                $scope.loading = true;
                $scope.pageNumber = parseInt(sessionStorage[ARTPAGE_STORAGE_KEY]);
                // var cur_val = $scope.pageNumber;
                // var new_val = cur_val + 1;
                $scope.AdvanceSearch = {
                    CategoryCode: $scope.activeCategory,
                    PageNumber: $scope.pageNumber,
                    PageSize: $scope.pageSize,
                    BasicSearch: false
                }

                getProduct($scope.AdvanceSearch);
                $scope.pageNumber = $scope.pageNumber + 1;

                sessionStorage[ARTPAGE_STORAGE_KEY] = $scope.pageNumber;

            } else if ($scope.toggleValue == 'ARC' && $scope.isMoreResultForArc) {

                $scope.loading = true;
                $scope.pageNumber = parseInt(sessionStorage[ARCPAGE_STORAGE_KEY]);
                // var cur_val = $scope.pageNumber;
                // var new_val = cur_val + 1;
                $scope.AdvanceSearch = {
                    CategoryCode: $scope.activeCategory,
                    PageNumber: $scope.pageNumber,
                    PageSize: $scope.pageSize,
                    BasicSearch: false
                }

                getProduct($scope.AdvanceSearch);
                $scope.pageNumber = $scope.pageNumber + 1;

                sessionStorage[ARCPAGE_STORAGE_KEY] = $scope.pageNumber;

            } else if ($scope.toggleValue == 'LIB' && $scope.isMoreResultForLib) {

                $scope.loading = true;
                $scope.pageNumber = parseInt(sessionStorage[LIBPAGE_STORAGE_KEY]);
                // var cur_val = $scope.pageNumber;
                // var new_val = cur_val + 1;
                $scope.AdvanceSearch = {
                    CategoryCode: $scope.activeCategory,
                    PageNumber: $scope.pageNumber,
                    PageSize: $scope.pageSize,
                    BasicSearch: false
                }

                getProduct($scope.AdvanceSearch);
                $scope.pageNumber = $scope.pageNumber + 1;

                sessionStorage[LIBPAGE_STORAGE_KEY] = $scope.pageNumber;
            }
            $scope.loadPage = false;
        }

    }

    // $scope.allGridArtData = [];
    // $scope.allGridArcData = [];
    // $scope.allGridLibData = [];

    function getProduct(filter) {
        var count = 0;
        $scope.loading = true;
        $scope.noResult = false;

        if ($scope.toggleValue == 'ALL') {
            ApiService.getproduct(filter)
                .then(function (r) {
                    if (r.data.Status === 'OK') {
                        $scope.allTabFirstTimeLoad = false;
                        var thumb = '';
                        if ($scope.allTabCategory == "Art" && r.data.Art.length > 0) {
                            $scope.data = r.data.Art;
                        } else if ($scope.allTabCategory == "Arc" && r.data.Arc.length > 0) {
                            $scope.data = r.data.Arc;
                        } else if ($scope.allTabCategory == "Lib" && r.data.Lib.length > 0) {
                            $scope.data = r.data.Lib;
                        } else if (r.data.Art.length <= 0 && r.data.Arc.length <= 0 && r.data.Lib.length <= 0) {
                            $scope.loading = false;
                            $scope.isMoreResultForAll = false;
                            return;
                        } else {
                            $scope.loadMore();
                        }

                        try {
                            $scope.allGridData = [].concat($scope.allGridData, $scope.data);

                            $allGrid.imagesLoaded({
                                //background: true
                            }).fail(function () {
                                $scope.loading = false;
                                $scope.loadPage = true;
                                $timeout(function () {
                                    $scope.imagesPreload = false;
                                }, 500);
                            }).done(function () {
                                $scope.loading = false;
                                $scope.loadPage = true;
                                $timeout(function () {
                                    $scope.imagesPreload = false;
                                }, 500);

                            });

                            $scope.loading = false;

                        } catch (ex) {
                            //$scope.isMoreResultForAll = false;
                            $scope.loading = false;
                            //$scope.noResult = true;
                            //$scope.AdvanceSearch.PageNumber = 1;
                        }
                    } else {
                        //$scope.isMoreResultForAll = false;
                        $scope.loading = false;
                        //$scope.noResult = true;
                    }

                });
        } else if ($scope.toggleValue == 'ART') {
            ApiService.getproduct(filter)
                .then(function (r) {
                    if (r.data.Status === 'OK') {
                        var thumb = '';
                        $scope.data = r.data.Products;
                        //$scope.allGridData.push(r.data.Products);
                        try {
                            $scope.data.forEach(function (item) {
                                $scope.artGridData.push(item);

                            });
                            $artGrid.imagesLoaded({
                                //background: true
                            }).fail(function () {
                                $scope.loading = false;
                                $scope.loadPage = true;
                                $timeout(function () {
                                    $scope.imagesPreload = false;
                                }, 500);
                            }).done(function () {
                                $scope.loading = false;
                                $scope.loadPage = true;
                                $timeout(function () {
                                    $scope.imagesPreload = false;
                                }, 500);

                            });


                            $scope.loading = false;

                        } catch (ex) {
                            $scope.isMoreResultForArt = false;
                            $scope.loading = false;
                            //$scope.noResult = true;
                            //$scope.AdvanceSearch.PageNumber = 1;
                        }
                    } else {
                        $scope.isMoreResultForArt = false;
                        $scope.loading = false;
                        //$scope.noResult = true;
                    }

                });
        } else if ($scope.toggleValue == 'ARC') {
            ApiService.getproduct(filter)
                .then(function (r) {
                    if (r.data.Status === 'OK') {
                        var thumb = '';
                        $scope.data = r.data.Products;
                        //$scope.allGridData.push(r.data.Products);
                        try {
                            $scope.data.forEach(function (item) {
                                $scope.arcGridData.push(item);

                            });
                            $arcGrid.imagesLoaded({
                                //background: true
                            }).fail(function () {
                                $scope.loading = false;
                                $scope.loadPage = true;
                                $timeout(function () {
                                    $scope.imagesPreload = false;
                                }, 500);
                            }).done(function () {
                                $scope.loading = false;
                                $scope.loadPage = true;
                                $timeout(function () {
                                    $scope.imagesPreload = false;
                                }, 500);

                            });

                            $scope.loading = false;

                        } catch (ex) {
                            $scope.isMoreResultForArc = false;
                            $scope.loading = false;
                            //$scope.noResult = true;
                            //$scope.AdvanceSearch.PageNumber = 1;
                        }
                    } else {
                        $scope.isMoreResultForArc = false;
                        $scope.loading = false;
                        //$scope.noResult = true;
                    }

                });
        } else if ($scope.toggleValue == 'LIB') {
            ApiService.getproduct(filter)
                .then(function (r) {
                    if (r.data.Status === 'OK') {
                        var thumb = '';
                        $scope.data = r.data.Products;
                        //$scope.allGridData.push(r.data.Products);
                        try {
                            $scope.data.forEach(function (item) {
                                $scope.libGridData.push(item);

                            });
                            $libGrid.imagesLoaded({
                                //background: true
                            }).fail(function () {
                                $scope.loading = false;
                                $scope.loadPage = true;
                                $timeout(function () {
                                    $scope.imagesPreload = false;
                                }, 500);
                            }).done(function () {
                                $scope.loading = false;
                                $scope.loadPage = true;
                                $timeout(function () {
                                    $scope.imagesPreload = false;
                                }, 500);

                            });

                            $scope.loading = false;

                        } catch (ex) {
                            $scope.isMoreResultForLib = false;
                            $scope.loading = false;
                            //$scope.noResult = true;
                            //$scope.AdvanceSearch.PageNumber = 1;
                        }
                    } else {
                        $scope.isMoreResultForLib = false;
                        $scope.loading = false;
                        //$scope.noResult = true;
                    }

                });
        }

    }

    $scope.collectionSelection = function () {
        $timeout(function () {
            angular.element('.bs_AdvOptionsCollectionList').show();
        }, 300);
    }

    $scope.clickToCollectionField = function (val) {
        $scope.libFormData.Collection = val;
        angular.element('.bs_AdvOptionsCollectionList').hide();
    }

    $scope.autoComplete = function (keyword) {
        $scope.keywords = {
            Search: keyword,
            HistorySearch: '',
            ItemCount: 7
        }
        $timeout(function () {
            ApiService.GetProductAutoComplete($scope.keywords).then(function (r) {
                angular.element('.bs_autocomplete').show();
                $scope.autoData = r.data;
                $scope.autoCompleteLength = $scope.autoData.Results.length;
            });
        }, 1000);

    }

    $scope.clickToBasicSearch = function (keyword) {
        $scope.genericSearch.Search = keyword;
        //$scope.autoComplete(keyword);
        $scope.basicSearch();
    }

    $scope.artTabActive = true;
    $scope.searchTab = function (value) {
        $scope.filterValue = value;
        if (value == 'art') {
            $scope.artTabActive = true;
            $scope.libTabActive = false;
            $scope.arcTabActive = false;
        } else if (value == 'lib') {
            $scope.artTabActive = false;
            $scope.libTabActive = true;
            $scope.arcTabActive = false;
        } else {
            $scope.artTabActive = false;
            $scope.libTabActive = false;
            $scope.arcTabActive = true;
        }
    }

    // $scope.filter = {
    //     artwork: true,
    //     archives: true,
    //     library: true
    // }

    // $scope.activeCategory = ["ART", "LIB", "ARC"];

    // $scope.item_filter = function(category) {
    //     //$grid.imagesLoaded(function() {
    //     $grid.html('<div class="grid-sizer"></div>');
    //     //});
    //     var index = $scope.activeCategory.indexOf(category);
    //     if (index !== -1) {
    //         $scope.activeCategory.splice(index, 1);
    //     } else {
    //         $scope.activeCategory.push(category);
    //     }

    //     $scope.AdvanceSearch = {
    //         CategoryCode: $scope.activeCategory,
    //         PageNumber: 1,
    //         PageSize: 10
    //     }
    //     getProduct($scope.AdvanceSearch);

    // }

    $scope.artFormData = {
        Artist: '',
        ArtistDateOfBirth: '',
        ArtistDateOfDeath: '',
        SourceTitle: '',
        SourceNo: '',
        ProductDate: '',
        Medium: '',
        Creditline: '',
        Location: ''
    }

    $scope.libFormData = {
        SourceTitle: '',
        Artist: '',
        ProductDate: '',
        Imprint: '',
        Location: '',
        Language: '',
        MeetingName: '',
        SeriesTitle: '',
        ISBN: '',
        ISSN: '',
        DeweyClass: '',

    }

    $scope.arcFormData = {
        SourceTitle: '',
        Artist: '',
        SourceNo: '',
        Subject: '',
        Language: '',
        ProductDate: '',
        FondsCollection: '',
        ScopeAndContent: ''
    }


    $scope.AdvSearch = function () {
        sessionStorage[FILTER_STORAGE_KEY] = 'a';
        sessionStorage[RELATEDITEM_STORAGE_KEY] = false;
        angular.element(".search-dropdown").toggleClass("avtive");
        angular.element(".icon-advanceSearch").toggleClass("active");
        if ($scope.artTabActive) {
            sessionStorage[ADVSEARCHCATE_STORAGE_KEY] = 'ART';
            sessionStorage[JSON_ADVSEARCH_STORAGE_KEY] = JSON.stringify($scope.artFormData);
            $state.go("search", {
                'Search': '',
                'SearchType': 'a',
                'CategoryCode': 'ART',
                'SourceTitle': $scope.artFormData.SourceTitle,
                'SourceNo': $scope.artFormData.SourceNo,
                'Artist': $scope.artFormData.Artist,
                'Creditline': $scope.artFormData.Creditline,
                'ProductDate': $scope.artFormData.ProductDate,
                'Medium': $scope.artFormData.Medium,
                'Location': $scope.artFormData.Location,
                'det': null
            });

        } else if ($scope.libTabActive) {
            sessionStorage[ADVSEARCHCATE_STORAGE_KEY] = 'LIB';
            sessionStorage[JSON_ADVSEARCH_STORAGE_KEY] = JSON.stringify($scope.libFormData);
            $state.go("search", {
                'Search': '',
                'SearchType': 'a',
                'CategoryCode': 'LIB',
                'SourceTitle': $scope.libFormData.SourceTitle,
                'Artist': $scope.libFormData.Artist,
                'Location': $scope.libFormData.Location,
                'Language': $scope.libFormData.Language,
                'MeetingName': $scope.libFormData.MeetingName,
                'Imprint': $scope.libFormData.Imprint,
                'SeriesTitle': $scope.libFormData.SeriesTitle,
                'ISBN': $scope.libFormData.ISBN,
                'ISSN': $scope.libFormData.ISSN,
                'DeweyClass': $scope.libFormData.DeweyClass,
                'ProductDate': $scope.libFormData.ProductDate,
                'Subject': $scope.libFormData.Subject,
                'det': null
            });
        } else if ($scope.arcTabActive) {
            sessionStorage[ADVSEARCHCATE_STORAGE_KEY] = 'ARC';
            sessionStorage[JSON_ADVSEARCH_STORAGE_KEY] = JSON.stringify($scope.arcFormData);
            $state.go("search", {
                'Search': '',
                'SearchType': 'a',
                'CategoryCode': 'ARC',
                'SourceTitle': $scope.arcFormData.SourceTitle,
                'Artist': $scope.arcFormData.Artist,
                'Subject': $scope.arcFormData.Subject,
                'Language': $scope.arcFormData.Language,
                'ProductDate': $scope.arcFormData.ProductDate,
                'FondsCollection': $scope.arcFormData.FondsCollection,
                'ScopeAndContent': $scope.arcFormData.ScopeAndContent,
                'det': null
            });
        }
    }

    $scope.open_advanceSearch = function () {
        angular.element(".search-dropdown").toggleClass("avtive");
        angular.element(".icon-advanceSearch").toggleClass("active");
    }

    $scope.toggleValue = 'ALL';
    $scope.activeCategory = ["ART", "ARC", "LIB"];
    $scope.$parent.switchPlaceholder = 'Search across Artwork, Archive & Library items...';
    $scope.switchBG = function (category) {

        $(document).scrollTop($(document).height());
        $scope.loadPage = true;
        //$scope.isMoreResult = true;
        $scope.toggleValue = category;
        $scope.activeCategory = [category];
        if ($scope.toggleValue == 'ART') {
            $scope.artTabActive = true;
            $scope.libTabActive = false;
            $scope.arcTabActive = false;
            $scope.siteLoc = 'artwork';

            // if (sessionStorage[ARTPAGE_STORAGE_KEY] == 1) {
            //     $scope.imagesPreload = true;
            //     $scope.loadMore();

            // }
        } else if ($scope.toggleValue == 'ARC') {
            $scope.artTabActive = false;
            $scope.libTabActive = false;
            $scope.arcTabActive = true;
            $scope.siteLoc = 'rotunda';
            // if (sessionStorage[ARCPAGE_STORAGE_KEY] == 1) {
            //     $scope.imagesPreload = true;
            //     $scope.loadMore();

            // }

        } else if ($scope.toggleValue == 'LIB') {
            $scope.artTabActive = false;
            $scope.libTabActive = true;
            $scope.arcTabActive = false;
            $scope.siteLoc = 'rotunda';
            // if (sessionStorage[LIBPAGE_STORAGE_KEY] == 1) {
            //     $scope.imagesPreload = true;
            //     $scope.loadMore();
            // }

        } else if ($scope.toggleValue == 'ALL') {
            $scope.artTabActive = true;
            $scope.libTabActive = false;
            $scope.arcTabActive = false;
            $scope.activeCategory = ["ART", "ARC", "LIB"];

            // if (sessionStorage[ALLPAGE_STORAGE_KEY] == 1) {
            //     $scope.imagesPreload = true;
            //     $scope.loadMore();

            // }

        }
        $scope.init();

    }

    $scope.scrollToCollection = function () {
        $window.scrollTo(0, 2650);
    }

    $scope.navigateTo = function (loc) {
        anchorSmoothScroll.scrollTo(loc);
    }

    $scope.goToPage = function (location, from, id) {
        $state.go(location, {
            'from': from,
            'productId': id
        }, {
            reload: true
        });
    }


}]);

app.controller('searchCtrl', ['$scope', '$state', '$stateParams', '$location', '$http', '$timeout', 'ApiService', '$compile', '$window', 'useragentProperties', function ($scope, $state, $stateParams, $location, $http, $timeout, ApiService, $compile, $window, useragentProperties) {
    $scope.device = useragentProperties.touch;
    $scope.$parent.location = 'search';
    $scope.$parent.homeTop = true;
    $scope.$parent.hasOptionBar = true;
    $scope.isGridView = true;
    $scope.isSort = false;
    $scope.isGrid = true;
    // $scope.allTabFirstTimeLoad = true;
    // $scope.allTabCategory = "Lib";
    $scope.toggleValue = 'ALL';
    $scope.categoryOption = 'ALL';
    $scope.categoryName = $stateParams.CategoryCode;
    $scope.artWorkFilterTab = true;

    $scope.glViewArt = true;
    $scope.glViewArc = true;
    $scope.glViewLib = false;
    $scope.glViewOther = true;

    $scope.isMoreResultForArt = true;
    $scope.isMoreResultForAll = true;
    $scope.isMoreResultForArc = true;
    $scope.isMoreResultForLib = true;

    $scope.sorting_func = null;
    $scope.sorting_type = null;
    $scope.sorting_order = null;
    $scope.keyWord = "";

    if(DOWNTIME){
        $state.go('downtime');
    }

    var $allGrid = angular.element('.allGrid');
    var $artGrid = angular.element('.artGrid');
    var $libGrid = angular.element('.libGrid');
    var $arcGrid = angular.element('.arcGrid');
    var $grid = angular.element('.grid-search');

    var $allList = angular.element('.allList-search');
    var $artList = angular.element('.artList-search');
    var $arcList = angular.element('.arcList-search');
    var $libList = angular.element('.libList-search');
    var $list = angular.element('.list-search');

    if ($scope.device) {
        $scope.masonry = {
            //itemSelector: '.masonry-block',
            //horizontalOrder: false,
            //columnWidth: '.masonry-block',
            gutter: '.gutter-sizer',
            //initLayout: false,
            //animate: true,
            //percentPosition: true,
            transitionDuration: '0s',
        }
    } else {
        $scope.masonry = {
            //itemSelector: '.masonry-block',
            //horizontalOrder: false,
            //columnWidth: '.masonry-block',
            gutter: '.gutter-sizer',
            //initLayout: false,
            //animate: true,
            //percentPosition: true,
            transitionDuration: '0s',
        }
    }

    if ($stateParams.det) {
        sessionStorage.removeItem("ADVS");
        //sessionStorage[JSON_ADVSEARCH_STORAGE_KEY] = null;
    }

    if ($stateParams.relatedSearch) {
        sessionStorage[RELATEDITEM_STORAGE_KEY] = true;
    } else {
        sessionStorage[RELATEDITEM_STORAGE_KEY] = false;
    }

    $scope.artFormData = {
        Artist: '',
        ArtistDateOfBirth: '',
        ArtistDateOfDeath: '',
        SourceTitle: '',
        SourceNo: '',
        ProductDate: '',
        Medium: '',
        Creditline: '',
        Location: ''
    }

    $scope.libFormData = {
        SourceTitle: '',
        Artist: '',
        // MeetingName: '',
        ProductDate: '',
        Imprint: '',
        SeriesTitle: '',
        ISBN: '',
        ISSN: '',
        DeweyClass: '',
        Language: '',
        Subject: '',
        Collection: '',
        Location: ''
    }

    $scope.arcFormData = {
        SourceTitle: '',
        Artist: '',
        SourceNo: '',
        Subject: '',
        Language: '',
        ProductDate: '',
        FondsCollection: '',
        ScopeAndContent: ''
    }

    $scope.toggleValue = $stateParams.CategoryCode ? $stateParams.CategoryCode : 'ALL';
    $scope.categoryOption = $stateParams.CategoryCode ? $stateParams.CategoryCode : 'ALL';
    if ($scope.toggleValue == 'ART') {
        $scope.artTabActive = true;
        $scope.libTabActive = false;
        $scope.arcTabActive = false;

        $scope.artWorkFilterTab = true;
        $scope.libraryFilterTab = false;
        $scope.archiveFilterTab = false;

        sessionStorage[ADVSEARCHCATE_STORAGE_KEY] = 'ART';

        $scope.isGridView = $scope.glViewArt;
    } else if ($scope.toggleValue == 'ARC') {
        $scope.artTabActive = false;
        $scope.libTabActive = false;
        $scope.arcTabActive = true;

        $scope.artWorkFilterTab = false;
        $scope.libraryFilterTab = false;
        $scope.archiveFilterTab = true;

        sessionStorage[ADVSEARCHCATE_STORAGE_KEY] = 'ARC';

        $scope.isGridView = $scope.glViewArc;
    } else if ($scope.toggleValue == 'LIB') {
        $scope.artTabActive = false;
        $scope.libTabActive = true;
        $scope.arcTabActive = false;

        $scope.artWorkFilterTab = false;
        $scope.libraryFilterTab = true;
        $scope.archiveFilterTab = false;

        sessionStorage[ADVSEARCHCATE_STORAGE_KEY] = 'LIB';

        $scope.isGridView = $scope.glViewLib;
    } else if ($scope.toggleValue == 'ALL') {
        $scope.artTabActive = true;
        $scope.libTabActive = false;
        $scope.arcTabActive = false;

        $scope.artWorkFilterTab = true;
        $scope.libraryFilterTab = false;
        $scope.archiveFilterTab = false;

        $scope.isGridView = $scope.glViewOther;
    }

    $scope.back = function () {
        $window.history.back();
    }

    $scope.genericSearch = {
        Search: ''
    }
    sessionStorage[JSON_BSSEARCH_STORAGE_KEY] = $stateParams.Search ? $stateParams.Search.replace(/(<([^>]+)>)/ig, "") : '';
    $scope.genericSearch.Search = $stateParams.Search ? $stateParams.Search.replace(/(<([^>]+)>)/ig, "") : '';

    $scope.display_list = $stateParams.OnDisplay ? true : false;
    $scope.imageAvailability_list = $stateParams.ImageAvailability ? true : false;
    $scope.showANDlanguage_list = $stateParams.FilterLanguageOperator ? true : false;
    $scope.filter_Subject = $stateParams.FilterSubject ? $stateParams.FilterSubject : '';
    $scope.AdvanceSearch = {
        Search: '',
        BasicSearch: null,
        SourceTitle: null,
        SourceNo: null,
        CategoryCode: null,
        Artist: null,
        ProductDate: null,
        Imprint: null,
        Creditline: null,
        Collection: null,
        MaterialType: null,
        Medium: null,
        Location: null,
        Subject: null,
        Language: null,
        MeetingName: null,
        SeriesTitle: null,
        ISBN: null,
        ISSN: null,
        DeweyClass: null,
        AccessLevel: null,
        StorageLocation: null,
        OnDisplay: null,
        FilterLanguage: null,
        FilterArtist: null,
        FilterLocation: null,
        FilterSubject: null,
        ItemAvailability: null,
        ImageAvailability: null,
        FilterLanguageOperator: null,
        FilterMedium: null
    }

    sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY] = JSON.stringify({
        'MaterialType': null,
        'Collection': null,
        'AccessLevel': null,
        'StorageLocation': null,
        'OnDisplay': null,
        'FilterLanguage': null,
        'FilterArtist': null,
        'FilterLocation': null,
        'FilterSubject': null,
        'ItemAvailability': null,
        'ImageAvailability': null,
        'FilterLanguageOperator': null,
        'FilterMedium': null
    });
    sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY] = JSON.stringify({

        'MaterialType': $stateParams.MaterialType ? $stateParams.MaterialType : null,
        'Collection': $stateParams.Collection ? $stateParams.Collection : null,
        'AccessLevel': $stateParams.AccessLevel ? $stateParams.AccessLevel : null,
        'StorageLocation': $stateParams.StorageLocation ? $stateParams.StorageLocation : null,
        'OnDisplay': $stateParams.OnDisplay ? $stateParams.OnDisplay : null,
        'FilterLanguage': $stateParams.FilterLanguage ? $stateParams.FilterLanguage : null,
        'FilterArtist': $stateParams.FilterArtist ? $stateParams.FilterArtist : null,
        'FilterLocation': $stateParams.FilterLocation ? $stateParams.FilterLocation : null,
        'FilterSubject': $stateParams.FilterSubject ? $stateParams.FilterSubject : null,
        'ItemAvailability': $stateParams.ItemAvailability ? $stateParams.ItemAvailability : null,
        'ImageAvailability': $stateParams.ImageAvailability ? $stateParams.ImageAvailability : null,
        'FilterLanguageOperator': $stateParams.FilterLanguageOperator ? $stateParams.FilterLanguageOperator : null,
        'FilterMedium': $stateParams.FilterMedium ? $stateParams.FilterMedium : null
    });

    $scope.searchData = function (page, isAdvance, sort, cate) {

        if (isAdvance == 'a') {
            if (cate == 'ART') {
                if (sessionStorage[JSON_ADVSEARCH_STORAGE_KEY]) {
                    $scope.artFormData = JSON.parse(sessionStorage[JSON_ADVSEARCH_STORAGE_KEY]);
                } else {
                    $scope.artFormData = {
                        Artist: $stateParams.Artist,
                        SourceTitle: $stateParams.SourceTitle,
                        Medium: $stateParams.Medium,
                        Creditline: $scope.artFormData.Creditline,
                        SourceNo: $stateParams.SourceNo,
                        ProductDate: $stateParams.ProductDate,
                        Location: $stateParams.Location
                    }
                }

                $scope.AdvanceSearch = {
                    PageNumber: page,
                    PageSize: $scope.pageSize,
                    Search: null,
                    BasicSearch: false,
                    RelatedSearch: sessionStorage[RELATEDITEM_STORAGE_KEY],
                    CategoryCode: ['ART'],
                    ProductId: sessionStorage[RELATEDITEM_STORAGE_KEY] ? $stateParams.ProductId : null,
                    SourceTitle: $scope.artFormData.SourceTitle ? $scope.artFormData.SourceTitle : null,
                    SourceNo: $scope.artFormData.SourceNo ? $scope.artFormData.SourceNo : null,
                    Artist: $scope.artFormData.Artist ? $scope.artFormData.Artist : null,
                    Subject: sessionStorage[RELATEDITEM_STORAGE_KEY] ? $stateParams.Subject : null,
                    ProductDate: $scope.artFormData.ProductDate ? $scope.artFormData.ProductDate : null,
                    Creditline: $scope.artFormData.Creditline ? $scope.artFormData.Creditline : null,
                    Medium: $scope.artFormData.Medium ? $scope.artFormData.Medium : null,
                    Location: $scope.artFormData.Location ? $scope.artFormData.Location : null,
                    OnDisplay: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).OnDisplay ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).OnDisplay : $scope.display_list ? $scope.display_list : null,
                    AccessLevel: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).AccessLevel ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).AccessLevel : $scope.getAccessList.length > 0 ? $scope.access_list : null,
                    Sort: sort,
                    FilterArtist: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterArtist ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterArtist : $scope.getArtistList.length > 0 ? $scope.artist_list : null,

                    FilterLocation: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterLocation ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterLocation : $scope.getLocList.length > 0 ? $scope.location_list : null,
                    FilterMedium: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterMedium ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterMedium : $scope.getMediumList.length > 0 ? $scope.medium_list : null,
                    ImageAvailability: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).ImageAvailability ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).ImageAvailability : $scope.imageAvailability_list ? 'available' : null,
                    Sort: $scope.sorting_type ? $scope.sorting_type : null,
                    Order: $scope.sorting_order ? $scope.sorting_order : null

                }

            } else if (cate == 'LIB') {

                if (sessionStorage[JSON_ADVSEARCH_STORAGE_KEY]) {

                    $scope.libFormData = JSON.parse(sessionStorage[JSON_ADVSEARCH_STORAGE_KEY]);
                } else {

                    $scope.libFormData = {
                        SourceTitle: $stateParams.SourceTitle,
                        Artist: $stateParams.Artist,
                        ProductDate: $stateParams.ProductDate,
                        Imprint: $stateParams.Imprint,
                        Location: $stateParams.Location,
                        Language: $stateParams.Language,
                        //MeetingName: $stateParams.MeetingName,
                        SeriesTitle: $stateParams.SeriesTitle,
                        ISBN: $stateParams.ISBN,
                        ISSN: $stateParams.ISSN,
                        DeweyClass: $stateParams.DeweyClass,
                        Subject: $stateParams.Subject,
                        Collection: $stateParams.Collection,

                    }
                }

                $scope.AdvanceSearch = {
                    PageNumber: page,
                    PageSize: $scope.pageSize,
                    Search: null,
                    BasicSearch: false,
                    RelatedSearch: sessionStorage[RELATEDITEM_STORAGE_KEY],
                    CategoryCode: ['LIB'],
                    ProductId: sessionStorage[RELATEDITEM_STORAGE_KEY] ? $stateParams.ProductId : null,
                    SourceTitle: $scope.libFormData.SourceTitle ? $scope.libFormData.SourceTitle : null,
                    Artist: $scope.libFormData.Artist ? $scope.libFormData.Artist : null,
                    ProductDate: $scope.libFormData.ProductDate ? $scope.libFormData.ProductDate : null,
                    Imprint: $scope.libFormData.Imprint ? $scope.libFormData.Imprint : null,
                    Location: $scope.libFormData.Location ? $scope.libFormData.Location : null,
                    Language: $scope.libFormData.Language ? $scope.libFormData.Language : null,
                    MeetingName: $scope.libFormData.MeetingName ? $scope.libFormData.MeetingName : null,
                    SeriesTitle: $scope.libFormData.SeriesTitle ? $scope.libFormData.SeriesTitle : null,
                    ISBN: $scope.libFormData.ISBN ? $scope.libFormData.ISBN : null,
                    ISSN: $scope.libFormData.ISSN ? $scope.libFormData.ISSN : null,
                    DeweyClass: $scope.libFormData.DeweyClass ? $scope.libFormData.DeweyClass : null,
                    Subject: $scope.libFormData.Subject ? $scope.libFormData.Subject : null,
                    Collection: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).Collection ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).Collection : $scope.libFormData.Collection ? $scope.libFormData.Collection : $scope.getColList.length > 0 ? $scope.collections_list : null,
                    Sort: sort,
                    FilterArtist: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterArtist ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterArtist : $scope.getAuthorList.length > 0 ? $scope.author_list : null,
                    FilterLocation: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterLocation ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterLocation : $scope.getCountryList.length > 0 ? $scope.country_list : null,
                    FilterLanguage: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterLanguage ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterLanguage : $scope.getLibLangList.length > 0 ? $scope.filter_LibLanguage : null,
                    FilterSubject: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterSubject ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterSubject : $scope.filter_Subject ? $scope.filter_Subject : null,
                    ItemAvailability: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).ItemAvailability ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).ItemAvailability : $scope.getItemAvailabilityList.length > 0 ? $scope.itemAvailability_list : null,
                    FilterLanguageOperator: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterLanguageOperator ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterLanguageOperator : $scope.showANDlanguage_list ? 'and' : null,
                    Sort: $scope.sorting_type ? $scope.sorting_type : null,
                    Order: $scope.sorting_order ? $scope.sorting_order : null

                }


            } else if (cate == 'ARC') {

                if (sessionStorage[JSON_ADVSEARCH_STORAGE_KEY]) {
                    $scope.arcFormData = JSON.parse(sessionStorage[JSON_ADVSEARCH_STORAGE_KEY]);
                } else {
                    $scope.arcFormData = {
                        SourceTitle: $stateParams.SourceTitle,
                        Artist: $stateParams.Artist,
                        SourceNo: $stateParams.SourceNo,
                        Subject: $stateParams.Subject,
                        Language: $stateParams.Language,
                        ProductDate: $stateParams.ProductDate,
                        FondsCollection: $stateParams.FondsCollection,
                        ScopeAndContent: $stateParams.ScopeAndContent
                    }
                }

                $scope.AdvanceSearch = {
                    PageNumber: page,
                    PageSize: $scope.pageSize,
                    Search: null,
                    BasicSearch: false,
                    RelatedSearch: sessionStorage[RELATEDITEM_STORAGE_KEY],
                    CategoryCode: ['ARC'],
                    ProductId: sessionStorage[RELATEDITEM_STORAGE_KEY] ? $stateParams.ProductId : null,
                    SourceTitle: $scope.arcFormData.SourceTitle ? $scope.arcFormData.SourceTitle : null,
                    // Artist: $scope.filter_artist ? $scope.filter_artist : $stateParams.Artist ? $stateParams.Artist : $scope.arcFormData.Artist ? $scope.arcFormData.Artist : null,
                    Artist: $scope.arcFormData.Artist ? $scope.arcFormData.Artist : null,
                    // Location: $scope.getLocList.length > 0 ? $scope.location_list : $stateParams.Location ? $stateParams.Location : $scope.arcFormData.Location ? $scope.arcFormData.Location : null,
                    SourceNo: $scope.arcFormData.SourceNo ? $scope.arcFormData.SourceNo : null,
                    ProductDate: $scope.arcFormData.ProductDate ? $scope.arcFormData.ProductDate : null,
                    Subject: $scope.arcFormData.Subject ? $scope.arcFormData.Subject : null,
                    FondsCollection: $scope.arcFormData.FondsCollection ? $scope.arcFormData.FondsCollection : '',
                    ScopeAndContent: $scope.arcFormData.ScopeAndContent ? $scope.arcFormData.ScopeAndContent : '',
                    Language: $scope.arcFormData.Language ? $scope.arcFormData.Language : null,
                    // OnDisplay: $scope.getDisplayList.length > 0 ? $scope.display_list : $stateParams.OnDisplay ? $stateParams.OnDisplay : null,
                    AccessLevel: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).AccessLevel ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).AccessLevel : $scope.getAccessList.length > 0 ? $scope.access_list : null,
                    MaterialType: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).MaterialType ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).MaterialType : $scope.getMaterialList.length > 0 ? $scope.material_list : null,
                    StorageLocation: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).StorageLocation ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).StorageLocation : $scope.getStoreLocList.length > 0 ? $scope.store_list : null,
                    Sort: sort,
                    FilterLanguage: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterLanguage ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterLanguage : $scope.getArcLangList.length > 0 ? $scope.filter_arcLanguage : null,
                    FilterLanguageOperator: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterLanguageOperator ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterLanguageOperator : $scope.showANDlanguage_list ? 'and' : null,
                    Sort: $scope.sorting_type ? $scope.sorting_type : null,
                    Order: $scope.sorting_order ? $scope.sorting_order : null

                }

            }

        } else if (isAdvance == 'b') {

            $scope.artFormData = {
                Artist: '',
                ArtistDateOfBirth: '',
                ArtistDateOfDeath: '',
                SourceTitle: '',
                SourceNo: '',
                ProductDate: '',
                Medium: '',
                Creditline: '',
                Location: ''
            }

            $scope.libFormData = {
                SourceTitle: '',
                Artist: '',
                // MeetingName: '',
                ProductDate: '',
                Imprint: '',
                SeriesTitle: '',
                ISBN: '',
                ISSN: '',
                DeweyClass: '',
                Language: '',
                Subject: '',
                Collection: '',
                Location: ''
            }

            $scope.arcFormData = {
                SourceTitle: '',
                Artist: '',
                SourceNo: '',
                Subject: '',
                Language: '',
                ProductDate: '',
                FondsCollection: '',
                ScopeAndContent: ''
            }
            $scope.AdvanceSearch = {
                PageNumber: page,
                PageSize: $scope.pageSize,
                BasicSearch: true,
                RelatedSearch: false,
                Search: $scope.genericSearch.Search ? $scope.genericSearch.Search : '',
                SourceTitle: null,
                SourceNo: null,
                CategoryCode: cate == 'ALL' ? ["ART", "ARC", "LIB"] : [cate],
                Artist: null,
                ProductDate: null,
                Imprint: null,
                Creditline: null,
                MaterialType: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).MaterialType ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).MaterialType : $scope.getMaterialList.length > 0 ? $scope.material_list : null,
                Medium: null,
                FondsCollection: null,
                ScopeAndContent: null,
                Location: null,
                Subject: null,
                Language: null,
                MeetingName: null,
                SeriesTitle: null,
                ISBN: null,
                ISSN: null,
                DeweyClass: null,
                Collection: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).Collection ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).Collection : $scope.getColList.length > 0 ? $scope.collections_list : null,
                AccessLevel: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).AccessLevel ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).AccessLevel : $scope.getAccessList.length > 0 ? $scope.access_list : null,
                StorageLocation: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).StorageLocation ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).StorageLocation : $scope.getStoreLocList.length > 0 ? $scope.store_list : null,
                OnDisplay: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).OnDisplay ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).OnDisplay : $scope.display_list ? $scope.display_list : null,
                Sort: sort,
                FilterLanguage: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterLanguage ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterLanguage : $scope.getArcLangList.length > 0 ? $scope.filter_arcLanguage : $scope.getLibLangList.length > 0 ? $scope.filter_LibLanguage : null,
                FilterArtist: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterArtist ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterArtist : $scope.getArtistList.length > 0 ? $scope.artist_list : $scope.getAuthorList.length > 0 ? $scope.author_list : null,
                FilterLocation: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterLocation ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterLocation : $scope.getCountryList.length > 0 ? $scope.country_list : $scope.getLocList.length > 0 ? $scope.location_list : null,
                FilterSubject: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterSubject ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterSubject : $scope.filter_Subject ? $scope.filter_Subject : null,
                ItemAvailability: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).ItemAvailability ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).ItemAvailability : $scope.getItemAvailabilityList.length > 0 ? $scope.itemAvailability_list : null,
                ImageAvailability: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).ImageAvailability ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).ImageAvailability : $scope.imageAvailability_list ? 'available' : null,
                FilterLanguageOperator: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterLanguageOperator ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterLanguageOperator : $scope.showANDlanguage_list ? 'and' : null,
                FilterMedium: JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterMedium ? JSON.parse(sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY]).FilterMedium : $scope.getMediumList.length > 0 ? $scope.medium_list : null,
                Sort: $scope.sorting_type ? $scope.sorting_type : null,
                Order: $scope.sorting_order ? $scope.sorting_order : null
            }
        }

        return $scope.AdvanceSearch;
    }


    $scope.init = function (isCountChange, isFilter) {
        $scope.imagesPreload = true;
        // $scope.allTabFirstTimeLoad = true;
        // $scope.allTabCategory = "Lib";

        $scope.allGridData = [];
        $scope.artGridData = [];
        $scope.arcGridData = [];
        $scope.libGridData = [];

        sessionStorage[ALLPAGE_STORAGE_KEY] = 1;
        sessionStorage[ARTPAGE_STORAGE_KEY] = 1;
        sessionStorage[ARCPAGE_STORAGE_KEY] = 1;
        sessionStorage[LIBPAGE_STORAGE_KEY] = 1;
        $scope.loadPage = true;
        $scope.loading = true;
        $scope.pageSize = 15;
        var sortVal = "";
        var isBasicSearch = "";
        //if ($scope.isMoreResult) {

        $scope.pageNumber = 1;

        if ($scope.isSort) {
            sortVal = "TITLE";
        } else {
            sortVal = "";
        }

        if (($scope.libFormData.Artist || $scope.arcFormData.Artist || $scope.artFormData.Artist || $stateParams.Artist) && ($stateParams.Location || $scope.libFormData.Location || $scope.arcFormData.Location || $scope.artFormData.Location)) {
            $scope.disabledLibFilter = true;
        } else {
            $scope.disabledLibFilter = false;
        }

        isBasicSearch = $scope.searchType ? $scope.searchType : $stateParams.SearchType;

        $scope.AdvanceSearch = $scope.searchData($scope.pageNumber, isBasicSearch, sortVal, $scope.toggleValue);

        if (!isFilter) {
            if ($scope.AdvanceSearch.Artist != null && $scope.AdvanceSearch.Artist != '') {
                $scope.keyWord = $scope.keyWord + "Artist: " + $scope.AdvanceSearch.Artist + ";";
            }
            if ($scope.AdvanceSearch.Creditline != null && $scope.AdvanceSearch.Creditline != '') {
                $scope.keyWord = $scope.keyWord + "Creditline: " + $scope.AdvanceSearch.Creditline + ";";
            }
            if ($scope.AdvanceSearch.Location != null && $scope.AdvanceSearch.Location != '') {
                $scope.keyWord = $scope.keyWord + "Location: " + $scope.AdvanceSearch.Location + ";";
            }
            if ($scope.AdvanceSearch.Medium != null && $scope.AdvanceSearch.Medium != '') {
                $scope.keyWord = $scope.keyWord + "Medium: " + $scope.AdvanceSearch.Medium + ";";
            }
            if ($scope.AdvanceSearch.ProductDate != null && $scope.AdvanceSearch.ProductDate != '') {
                $scope.keyWord = $scope.keyWord + "ProductDate: " + $scope.AdvanceSearch.ProductDate + ";";
            }
            if ($scope.AdvanceSearch.SourceNo != null && $scope.AdvanceSearch.SourceNo != '') {
                $scope.keyWord = $scope.keyWord + "SourceNo: " + $scope.AdvanceSearch.SourceNo + ";";
            }
            if ($scope.AdvanceSearch.SourceTitle != null && $scope.AdvanceSearch.SourceTitle != '') {
                $scope.keyWord = $scope.keyWord + "SourceTitle: " + $scope.AdvanceSearch.SourceTitle + ";";
            }
            if ($scope.AdvanceSearch.FondsCollection != null && $scope.AdvanceSearch.FondsCollection != '') {
                $scope.keyWord = $scope.keyWord + "FondsCollection: " + $scope.AdvanceSearch.FondsCollection + ";";
            }
            if ($scope.AdvanceSearch.ScopeAndContent != null && $scope.AdvanceSearch.ScopeAndContent != '') {
                $scope.keyWord = $scope.keyWord + "ScopeAndContent: " + $scope.AdvanceSearch.ScopeAndContent + ";";
            }
            if ($scope.AdvanceSearch.Subject != null && $scope.AdvanceSearch.Subject != '') {
                $scope.keyWord = $scope.keyWord + "Subject: " + $scope.AdvanceSearch.Subject + ";";
            }
            if ($scope.AdvanceSearch.Collection != null && $scope.AdvanceSearch.Collection != '') {
                $scope.keyWord = $scope.keyWord + "Collection: " + $scope.AdvanceSearch.Collection + ";";
            }
            if ($scope.AdvanceSearch.DeweyClass != null && $scope.AdvanceSearch.DeweyClass != '') {
                $scope.keyWord = $scope.keyWord + "DeweyClass: " + $scope.AdvanceSearch.DeweyClass + ";";
            }
            if ($scope.AdvanceSearch.ISBN != null && $scope.AdvanceSearch.ISBN != '') {
                $scope.keyWord = $scope.keyWord + "ISBN: " + $scope.AdvanceSearch.ISBN + ";";
            }
            if ($scope.AdvanceSearch.ISSN != null && $scope.AdvanceSearch.ISSN != '') {
                $scope.keyWord = $scope.keyWord + "ISSN: " + $scope.AdvanceSearch.ISSN + ";";
            }
            if ($scope.AdvanceSearch.Imprint != null && $scope.AdvanceSearch.Imprint != '') {
                $scope.keyWord = $scope.keyWord + "Imprint: " + $scope.AdvanceSearch.Imprint + ";";
            }
            if ($scope.AdvanceSearch.Language != null && $scope.AdvanceSearch.Language != '') {
                $scope.keyWord = $scope.keyWord + "Language: " + $scope.AdvanceSearch.Language + ";";
            }
            if ($scope.AdvanceSearch.Search != null && $scope.AdvanceSearch.Search != '') {
                $scope.keyWord = $scope.keyWord + "Search: " + $scope.AdvanceSearch.Search + ";";
            }
            ApiService.GetProductFilter($scope.AdvanceSearch).then(function (r) {
                if (r.data.Status === 'OK') {

                    $scope.dataArtworkArtists = r.data.ArtworkArtists;
                    $scope.dataLibraryAuthors = r.data.LibraryAuthors;
                    $scope.dataArtworkLocations = r.data.ArtworkLocations;
                    $scope.dataLibraryLocations = r.data.LibraryLocations;
                    $scope.dataLibraryLanguages = r.data.LibraryLanguages;
                    $scope.dataArchiveLanguages = r.data.ArchiveLanguages;
                    $scope.dataLocation = r.data.Locations;
                    $scope.dataMediums = r.data.Mediums;
                    $scope.dataMaterialTypes = r.data.MaterialTypes;
                    $scope.dataAccessLevels = r.data.AccessLevels;
                    $scope.dataStorageLocations = r.data.StorageLocations;
                    $scope.dataCollections = r.data.Collections;
                    $scope.dataOnDisplays = r.data.OnDisplays;
                    $scope.dataLibraryAvailability = r.data.LibraryAvailability;
                }
            });
        }

        if (!isCountChange) {
            ApiService.GetProductCount($scope.AdvanceSearch).then(function (r) {
                $scope.allTabCategory
                if (r.data.CountART <= 0) {
                    $scope.allTabCategory = "Art";
                } else if (r.data.CountART <= 0 && r.data.CountARC <= 0) {
                    $scope.allTabCategory = "Arc";
                } else {
                    $scope.allTabCategory = "Lib";
                }
                if (isBasicSearch == 'a') {

                    if ($scope.toggleValue == 'ART' && sessionStorage[ADVSEARCHCATE_STORAGE_KEY] == 'ART') {
                        $scope.productsLength = 0;
                        $scope.artLength = r.data.CountART;
                        $scope.arcLength = 0;
                        $scope.libLength = 0;
                        $scope.isMoreResultForArt = true;
                        $scope.isMoreResultForAll = false;
                        $scope.isMoreResultForArc = false;
                        $scope.isMoreResultForLib = false;

                    } else if ($scope.toggleValue == 'ARC' && sessionStorage[ADVSEARCHCATE_STORAGE_KEY] == 'ARC') {
                        $scope.productsLength = 0;
                        $scope.artLength = 0;
                        $scope.arcLength = r.data.CountARC;
                        $scope.libLength = 0;
                        $scope.isMoreResultForArt = false;
                        $scope.isMoreResultForAll = false;
                        $scope.isMoreResultForArc = true;
                        $scope.isMoreResultForLib = false;

                    } else if ($scope.toggleValue == 'LIB' && sessionStorage[ADVSEARCHCATE_STORAGE_KEY] == 'LIB') {
                        $scope.productsLength = 0;
                        $scope.artLength = 0;
                        $scope.arcLength = 0;
                        $scope.libLength = r.data.CountLIB;
                        $scope.isMoreResultForArt = false;
                        $scope.isMoreResultForAll = false;
                        $scope.isMoreResultForArc = false;
                        $scope.isMoreResultForLib = true;

                    }

                } else {
                    if (isFilter) {
                        if ($scope.toggleValue == 'ART' && sessionStorage[ADVSEARCHCATE_STORAGE_KEY] == 'ART') {
                            $scope.productsLength = 0;
                            $scope.artLength = r.data.CountART;
                            $scope.arcLength = 0;
                            $scope.libLength = 0;
                            $scope.isMoreResultForArt = true;
                            $scope.isMoreResultForAll = false;
                            $scope.isMoreResultForArc = false;
                            $scope.isMoreResultForLib = false;

                        } else if ($scope.toggleValue == 'ARC' && sessionStorage[ADVSEARCHCATE_STORAGE_KEY] == 'ARC') {
                            $scope.productsLength = 0;
                            $scope.artLength = 0;
                            $scope.arcLength = r.data.CountARC;
                            $scope.libLength = 0;
                            $scope.isMoreResultForArt = false;
                            $scope.isMoreResultForAll = false;
                            $scope.isMoreResultForArc = true;
                            $scope.isMoreResultForLib = false;

                        } else if ($scope.toggleValue == 'LIB' && sessionStorage[ADVSEARCHCATE_STORAGE_KEY] == 'LIB') {
                            $scope.productsLength = 0;
                            $scope.artLength = 0;
                            $scope.arcLength = 0;
                            $scope.libLength = r.data.CountLIB;
                            $scope.isMoreResultForArt = false;
                            $scope.isMoreResultForAll = false;
                            $scope.isMoreResultForArc = false;
                            $scope.isMoreResultForLib = true;

                        }
                    } else {
                        $scope.productsLength = r.data.CountActive;
                        $scope.artLength = r.data.CountART;
                        $scope.arcLength = r.data.CountARC;
                        $scope.libLength = r.data.CountLIB;
                        $scope.isMoreResultForArt = true;
                        $scope.isMoreResultForAll = true;
                        $scope.isMoreResultForArc = true;
                        $scope.isMoreResultForLib = true;
                    }

                }
                $scope.loadMore();
            });
        } else {
            $scope.loadMore();
        }
        // if (isCountChange) {

        //}
        //}
    }


    $scope.filterTab = function (value) {
        if (value == 'art') {
            $scope.artWorkFilterTab = true;
            $scope.libraryFilterTab = false;
            $scope.archiveFilterTab = false;
        } else if (value == 'lib') {
            $scope.artWorkFilterTab = false;
            $scope.libraryFilterTab = true;
            $scope.archiveFilterTab = false;
        } else {
            $scope.artWorkFilterTab = false;
            $scope.libraryFilterTab = false;
            $scope.archiveFilterTab = true;
        }
    }

    $scope.searchTab = function (value) {
        $scope.filterValue = value;
        if (value == 'art') {
            $scope.artTabActive = true;
            $scope.libTabActive = false;
            $scope.arcTabActive = false;
        } else if (value == 'lib') {
            $scope.artTabActive = false;
            $scope.libTabActive = true;
            $scope.arcTabActive = false;
        } else {
            $scope.artTabActive = false;
            $scope.libTabActive = false;
            $scope.arcTabActive = true;
        }
    }

    $scope.$parent.switchPlaceholder = 'Search across Artwork, Archive & Library items...';
    $scope.switchBG = function (category) {
        $scope.loadPage = true;
        $scope.imagesPreload = false;
        $scope.sorting_order = null;
        $scope.sorting_func = null;
        $scope.sorting_type = null;

        $(document).scrollTop($(document).height());
        //$scope.isMoreResult = true;
        $scope.categoryName = category == 'ALL' ? '' : category;
        $scope.toggleValue = category;
        $scope.categoryOption = category;
        if ($scope.toggleValue == 'ART') {
            $scope.artTabActive = true;
            $scope.libTabActive = false;
            $scope.arcTabActive = false;

            $scope.artWorkFilterTab = true;
            $scope.libraryFilterTab = false;
            $scope.archiveFilterTab = false;

            $scope.siteLoc = 'artwork';
            $scope.isGridView = $scope.glViewArt;
            // if (sessionStorage[ARTPAGE_STORAGE_KEY] == 1) {
            //     $scope.imagesPreload = true;
            //     $scope.loadMore();
            // }

        } else if ($scope.toggleValue == 'ARC') {
            $scope.artTabActive = false;
            $scope.libTabActive = false;
            $scope.arcTabActive = true;

            $scope.artWorkFilterTab = false;
            $scope.libraryFilterTab = false;
            $scope.archiveFilterTab = true;

            $scope.siteLoc = 'rotunda';
            $scope.isGridView = $scope.glViewArc;

            // if (sessionStorage[ARCPAGE_STORAGE_KEY] == 1) {
            //     $scope.imagesPreload = true;
            //     $scope.loadMore();

            // }
        } else if ($scope.toggleValue == 'LIB') {
            $scope.artTabActive = false;
            $scope.libTabActive = true;
            $scope.arcTabActive = false;

            $scope.artWorkFilterTab = false;
            $scope.libraryFilterTab = true;
            $scope.archiveFilterTab = false;
            $scope.siteLoc = 'rotunda';
            $scope.isGridView = $scope.glViewLib;

            // if (sessionStorage[LIBPAGE_STORAGE_KEY] == 1) {
            //     $scope.imagesPreload = true;
            //     $scope.loadMore();
            // }
        } else if ($scope.toggleValue == 'ALL') {
            $scope.artTabActive = true;
            $scope.libTabActive = false;
            $scope.arcTabActive = false;

            $scope.artWorkFilterTab = true;
            $scope.libraryFilterTab = false;
            $scope.archiveFilterTab = false;

            $scope.isGridView = $scope.glViewOther;

            // if (sessionStorage[ALLPAGE_STORAGE_KEY] == 1) {
            //     $scope.imagesPreload = true;
            //     $scope.loadMore();

            // }

        }
        //  $timeout(function() {
        //     $scope.imagesPreload = false;
        // }, 500);
        if ($scope.isSort) {
            sortVal = "TITLE";
        } else {
            sortVal = "";
        }
        $scope.init(false, false)
        isBasicSearch = $scope.searchType ? $scope.searchType : $stateParams.SearchType;

        $scope.AdvanceSearch = $scope.searchData($scope.pageNumber, isBasicSearch, sortVal, $scope.toggleValue);
        ApiService.GetProductFilter($scope.AdvanceSearch).then(function (r) {
            if (r.data.Status === 'OK') {

                $scope.dataArtworkArtists = r.data.ArtworkArtists;
                $scope.dataLibraryAuthors = r.data.LibraryAuthors;
                $scope.dataArtworkLocations = r.data.ArtworkLocations;
                $scope.dataLibraryLocations = r.data.LibraryLocations;
                $scope.dataLibraryLanguages = r.data.LibraryLanguages;
                $scope.dataArchiveLanguages = r.data.ArchiveLanguages;
                $scope.dataLocation = r.data.Locations;
                $scope.dataMediums = r.data.Mediums;
                $scope.dataMaterialTypes = r.data.MaterialTypes;
                $scope.dataAccessLevels = r.data.AccessLevels;
                $scope.dataStorageLocations = r.data.StorageLocations;
                $scope.dataCollections = r.data.Collections;
                $scope.dataOnDisplays = r.data.OnDisplays;
                $scope.dataLibraryAvailability = r.data.LibraryAvailability;
            }
        });
        $state.go("search", {
            'CategoryCode': $scope.categoryName,
        }, {
            notify: false
        });

    }

    $scope.autoComplete = function (keyword) {
        $scope.keywords = {
            Search: keyword,
            HistorySearch: '',
            ItemCount: 7
        }
        $timeout(function () {
            ApiService.GetProductAutoComplete($scope.keywords).then(function (r) {
                angular.element('.bs_autocomplete').show();
                $scope.autoData = r.data;
                $scope.autoCompleteLength = $scope.autoData.Results.length;

            });
        }, 1000);

    }

    $scope.clickToBasicSearch = function (keyword) {
        $scope.loading = true;
        $scope.genericSearch.Search = keyword;
        //$scope.autoComplete(keyword);
        $scope.basicSearch();
    }

    $scope.sortingFunc = function (val) {
        $scope.sorting_func = val;
        var splitOptions = val.split(',');
        $scope.sorting_type = splitOptions[0].replace(' ', '');
        $scope.sorting_order = splitOptions[1].replace(' ', '');
        $scope.init(false, false);
    }

    // $scope.autoArtistComplete = function(keyword) {
    //     $scope.keywords = {
    //         Search: keyword,
    //         ItemCount: 5
    //     }

    //     $timeout(function() {
    //         ApiService.GetArtistAutoComplete($scope.keywords).then(function(r) {
    //             angular.element('.bs_artistAutocomplete').show();
    //             $scope.autoArtistData = r.data;
    //         });
    //     }, 300);

    // }

    // $scope.autoAuthorComplete = function(keyword) {
    //     $scope.keywords = {
    //         Search: keyword,
    //         ItemCount: 5
    //     }

    //     $timeout(function() {
    //         ApiService.GetArtistAutoComplete($scope.keywords).then(function(r) {
    //             angular.element('.bs_authorAutocomplete').show();
    //             $scope.autoAuthorData = r.data;
    //         });
    //     }, 300);

    // }

    // $scope.clickToArtistSearch = function(keyword) {
    //     $scope.filter_artist = keyword;
    // }

    // $scope.clickToAuthorSearch = function(keyword) {
    //     $scope.filter_artist = keyword;
    // }

    $scope.goToPage = function (location, from, id) {
        $state.go(location, {
            'from': from,
            'productId': id
        });
    }

    $scope.FilterFunc = function () {
        $scope.loading = true;
        //$scope.searchType = 'b';
        if ($scope.artWorkFilterTab) {
            sessionStorage[ADVSEARCHCATE_STORAGE_KEY] = 'ART';
            $scope.toggleValue = 'ART';
        } else if ($scope.archiveFilterTab) {
            sessionStorage[ADVSEARCHCATE_STORAGE_KEY] = 'arc';
            $scope.toggleValue = 'ARC';
        } else if ($scope.libraryFilterTab) {
            sessionStorage[ADVSEARCHCATE_STORAGE_KEY] = 'lib';
            $scope.toggleValue = 'LIB';
        }
        $state.go("search", {
            //'SearchType': 'a',
            'FilterArtist': $scope.getArtistList.length > 0 ? $scope.artist_list : $scope.getAuthorList.length > 0 ? $scope.author_list : null,
            'FilterLanguage': $scope.getArcLangList.length > 0 ? $scope.filter_arcLanguage : $scope.getLibLangList.length > 0 ? $scope.filter_LibLanguage : null,
            'FilterLocation': $scope.getCountryList.length > 0 ? $scope.country_list : $scope.getLocList.length > 0 ? $scope.location_list : null,
            'FilterSubject': $scope.filter_Subject ? $scope.filter_Subject : null,
            'ImageAvailability': $scope.imageAvailability_list ? 'available' : null,
            'FilterLanguageOperator': $scope.showANDlanguage_list ? 'and' : null,
            'ItemAvailability': $scope.getItemAvailabilityList.length > 0 ? $scope.itemAvailability_list : null,
            'FilterMedium': $scope.getMediumList.length > 0 ? $scope.medium_list : null,
            'Collection': $scope.getColList.length > 0 ? $scope.collections_list : null,
            'AccessLevel': $scope.getAccessList.length > 0 ? $scope.access_list : null,
            'StorageLocation': $scope.getStoreLocList.length > 0 ? $scope.store_list : null,
            'OnDisplay': $scope.display_list ? $scope.display_list : '',
            'MaterialType': $scope.getMaterialList.length > 0 ? $scope.material_list : null

        }, {
            notify: false
        });
        sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY] = JSON.stringify({
            'FilterArtist': $scope.getArtistList.length > 0 ? $scope.artist_list : $scope.getAuthorList.length > 0 ? $scope.author_list : null,
            'FilterLanguage': $scope.getArcLangList.length > 0 ? $scope.filter_arcLanguage : $scope.getLibLangList.length > 0 ? $scope.filter_LibLanguage : null,
            'FilterLocation': $scope.getCountryList.length > 0 ? $scope.country_list : $scope.getLocList.length > 0 ? $scope.location_list : null,
            'FilterSubject': $scope.filter_Subject ? $scope.filter_Subject : null,
            'ImageAvailability': $scope.imageAvailability_list ? 'available' : null,
            'FilterLanguageOperator': $scope.showANDlanguage_list ? 'and' : null,
            'ItemAvailability': $scope.getItemAvailabilityList.length > 0 ? $scope.itemAvailability_list : null,
            'FilterMedium': $scope.getMediumList.length > 0 ? $scope.medium_list : null,
            'Collection': $scope.getColList.length > 0 ? $scope.collections_list : null,
            'AccessLevel': $scope.getAccessList.length > 0 ? $scope.access_list : null,
            'StorageLocation': $scope.getStoreLocList.length > 0 ? $scope.store_list : null,
            'OnDisplay': $scope.display_list ? $scope.display_list : null,
            'MaterialType': $scope.getMaterialList.length > 0 ? $scope.material_list : null
        });
        $scope.init(false, true);
    }

    $scope.showArtistFilterOpt = function (val) {
        angular.element('.block-filter').hide();
        if (val == 'on') {
            angular.element('.filter-artist').show();
        } else {
            angular.element('.filter-artist').hide();
        }
    }

    $scope.getArtistList = $stateParams.FilterArtist ? $stateParams.FilterArtist.split('; ') : [];

    $scope.artist_list = $scope.getArtistList.join(';');
    $scope.artistFilter = function (value, checked) {
        var idx = $scope.getArtistList.indexOf(value);
        if (idx >= 0 && !checked) {
            $scope.getArtistList.splice(idx, 1);
        }
        if (idx < 0 && checked) {
            $scope.getArtistList.push(value);
        }
        $scope.artist_list = $scope.getArtistList.join('; ');
    };

    $scope.showAuthorFilterOpt = function (val) {
        angular.element('.block-filter').hide();
        if (val == 'on') {
            angular.element('.filter-author').show();
        } else {
            angular.element('.filter-author').hide();
        }
    }

    $scope.getAuthorList = $stateParams.FilterArtist ? $stateParams.FilterArtist.split('; ') : [];

    $scope.author_list = $scope.getAuthorList.join('; ');
    $scope.authorFilter = function (value, checked) {
        var idx = $scope.getAuthorList.indexOf(value);
        if (idx >= 0 && !checked) {
            $scope.getAuthorList.splice(idx, 1);
        }
        if (idx < 0 && checked) {
            $scope.getAuthorList.push(value);
        }

        $scope.author_list = $scope.getAuthorList.join('; ');

    };

    $scope.showLocFilterOpt = function (val) {
        angular.element('.block-filter').hide();
        if (val == 'on') {
            angular.element('.filter-location').show();
        } else {
            angular.element('.filter-location').hide();
        }

    }

    $scope.getLocList = $stateParams.FilterLocation ? $stateParams.FilterLocation.split("; ") : [];
    $scope.location_list = $scope.getLocList.join('; ');
    $scope.locFilter = function (value, checked) {
        var idx = $scope.getLocList.indexOf(value);
        if (idx >= 0 && !checked) {
            $scope.getLocList.splice(idx, 1);
        }
        if (idx < 0 && checked) {
            $scope.getLocList.push(value);
        }

        $scope.location_list = $scope.getLocList.join('; ');
        //$scope.init();
    };

    $scope.showCountryFilterOpt = function (val) {
        angular.element('.block-filter').hide();
        if (val == 'on') {
            angular.element('.filter-country').show();
        } else {
            angular.element('.filter-country').hide();
        }

    }

    $scope.getCountryList = $stateParams.FilterLocation ? $stateParams.FilterLocation.split("; ") : [];
    $scope.country_list = $scope.getCountryList.join('; ');

    $scope.countryFilter = function (value, checked) {
        var idx = $scope.getCountryList.indexOf(value);
        if (idx >= 0 && !checked) {
            $scope.getCountryList.splice(idx, 1);
        }
        if (idx < 0 && checked) {
            $scope.getCountryList.push(value);
        }
        $scope.country_list = $scope.getCountryList.join('; ');
        //$scope.init();
    };

    $scope.showColFilterOpt = function (val) {
        angular.element('.block-filter').hide();
        if (val == 'on') {
            angular.element('.filter-collections').show();
        } else {
            angular.element('.filter-collections').hide();
        }

    }

    $scope.getColList = $stateParams.Collection ? $stateParams.Collection.split("; ") : [];
    $scope.collections_list = $scope.getColList.join('; ');

    $scope.colFilter = function (value, checked) {
        var idx = $scope.getColList.indexOf(value);
        if (idx >= 0 && !checked) {
            $scope.getColList.splice(idx, 1);
        }
        if (idx < 0 && checked) {
            $scope.getColList.push(value);
        }
        $scope.collections_list = $scope.getColList.join('; ');
        //$scope.init();
    };

    $scope.showLibLangFilterOpt = function (val) {
        angular.element('.block-filter').hide();
        if (val == 'on') {
            angular.element('.filter-libLang').show();
        } else {
            angular.element('.filter-libLang').hide();
        }
    }

    $scope.getLibLangList = $stateParams.FilterLanguage ? $stateParams.FilterLanguage.split("; ") : [];
    $scope.filter_LibLanguage = $scope.getLibLangList.join('; ');

    $scope.libLangFilter = function (value, checked) {
        var idx = $scope.getLibLangList.indexOf(value);
        if (idx >= 0 && !checked) {
            $scope.getLibLangList.splice(idx, 1);
        }
        if (idx < 0 && checked) {
            $scope.getLibLangList.push(value);
        }
        $scope.filter_LibLanguage = $scope.getLibLangList.join('; ');

    };

    $scope.showArcLangFilterOpt = function (val) {
        angular.element('.block-filter').hide();
        if (val == 'on') {
            angular.element('.filter-arcLang').show();
        } else {
            angular.element('.filter-arcLang').hide();
        }
    }

    $scope.getArcLangList = $stateParams.FilterLanguage ? $stateParams.FilterLanguage.split("; ") : [];
    $scope.filter_arcLanguage = $scope.getArcLangList.join('; ');
    $scope.arcLangFilter = function (value, checked) {

        var idx = $scope.getArcLangList.indexOf(value);
        if (idx >= 0 && !checked) {
            $scope.getArcLangList.splice(idx, 1);
        }
        if (idx < 0 && checked) {
            $scope.getArcLangList.push(value);
        }
        $scope.filter_arcLanguage = $scope.getArcLangList.join('; ');

    };

    $scope.showMedFilterOpt = function (val) {
        angular.element('.block-filter').hide();
        if (val == 'on') {
            angular.element('.filter-medium').show();
        } else {
            angular.element('.filter-medium').hide();
        }
    }

    $scope.getMediumList = $stateParams.FilterMedium ? $stateParams.FilterMedium.split("; ") : [];
    $scope.medium_list = $scope.getMediumList.join('; ');
    $scope.medFilter = function (value, checked) {

        var idx = $scope.getMediumList.indexOf(value);
        if (idx >= 0 && !checked) {
            $scope.getMediumList.splice(idx, 1);
        }
        if (idx < 0 && checked) {
            $scope.getMediumList.push(value);
        }

        $scope.medium_list = $scope.getMediumList.join('; ');



    };

    // $scope.showDisFilterOpt = function(val) {
    //     if (val == 'on') {
    //         angular.element('.filter-display').show();
    //     } else {
    //         angular.element('.filter-display').hide();
    //     }
    // }

    // $scope.displayOption = [{
    //     option: 'On'
    // }, {
    //     option: 'Off'
    // }];
    // $scope.getDisplayList = [];
    // $scope.disFilter = function(dis) {

    //     var removeDisplaylist = [];

    //     if (dis.checked === true) {
    //         $scope.getDisplayList.push(dis.option);

    //     } else {
    //         removeDisplaylist.push(dis.option);
    //         var i = $scope.getDisplayList.length;
    //         while (i--) {
    //             if (removeDisplaylist.indexOf($scope.getDisplayList[i]) != -1) {
    //                 $scope.getDisplayList.splice(i, 1);
    //             }
    //         }

    //     }
    //     $scope.display_list = $scope.getDisplayList.join('; ');

    // };

    $scope.showMaterialFilterOpt = function (val) {
        angular.element('.block-filter').hide();
        if (val == 'on') {
            angular.element('.filter-material').show();
        } else {
            angular.element('.filter-material').hide();
        }
    }

    $scope.getMaterialList = $stateParams.MaterialType ? $stateParams.MaterialType.split("; ") : [];

    $scope.material_list = $scope.getMaterialList.join('; ');

    $scope.matFilter = function (value, checked) {

        var idx = $scope.getMaterialList.indexOf(value);
        if (idx >= 0 && !checked) {
            $scope.getMaterialList.splice(idx, 1);
        }
        if (idx < 0 && checked) {
            $scope.getMaterialList.push(value);
        }
        $scope.material_list = $scope.getMaterialList.join('; ');

    };

    // $scope.showImageAvailabilityFilterOpt = function(val) {
    //     if (val == 'on') {
    //         angular.element('.filter-imageAvailability').show();
    //     } else {
    //         angular.element('.filter-imageAvailability').hide();
    //     }
    // }

    // $scope.dataImageAvailability = [{
    //     item: 'Show available only'
    // }];

    // $scope.getImageAvailabilityList = [];
    // $scope.imageAvailabilityFilter = function(imgAvailable) {

    //     var removeImageAvailabilitylist = [];

    //     if (imgAvailable.checked === true) {
    //         $scope.getImageAvailabilityList.push('available');

    //     } else {
    //         removeImageAvailabilitylist.push('available');
    //         var i = $scope.getImageAvailabilityList.length;
    //         while (i--) {
    //             if (removeImageAvailabilitylist.indexOf($scope.getImageAvailabilityList[i]) != -1) {
    //                 $scope.getImageAvailabilityList.splice(i, 1);
    //             }
    //         }

    //     }
    //     $scope.imageAvailability_list = $scope.getImageAvailabilityList.join('; ');
    // };


    $scope.showItemAvailabilityFilterOpt = function (val) {
        angular.element('.block-filter').hide();
        if (val == 'on') {
            angular.element('.filter-itemAvailability').show();
        } else {
            angular.element('.filter-itemAvailability').hide();
        }
    }

    $scope.getItemAvailabilityList = $stateParams.ItemAvailability ? $stateParams.ItemAvailability.split("; ") : [];

    $scope.itemAvailability_list = $scope.getItemAvailabilityList.join('; ');

    $scope.itemAvailabilityFilter = function (value, checked) {

        var idx = $scope.getItemAvailabilityList.indexOf(value);
        if (idx >= 0 && !checked) {
            $scope.getItemAvailabilityList.splice(idx, 1);
        }
        if (idx < 0 && checked) {
            $scope.getItemAvailabilityList.push(value);
        }
        $scope.itemAvailability_list = $scope.getItemAvailabilityList.join('; ');
    };


    $scope.showAccessFilterOpt = function (val) {
        angular.element('.block-filter').hide();
        if (val == 'on') {
            angular.element('.filter-access').show();
        } else {
            angular.element('.filter-access').hide();
        }
    }

    $scope.getAccessList = $stateParams.AccessLevel ? $stateParams.AccessLevel.split("; ") : [];
    $scope.access_list = $scope.getAccessList.join('; ');

    $scope.accessFilter = function (value, checked) {

        var idx = $scope.getAccessList.indexOf(value);
        if (idx >= 0 && !checked) {
            $scope.getAccessList.splice(idx, 1);
        }
        if (idx < 0 && checked) {
            $scope.getAccessList.push(value);
        }
        $scope.access_list = $scope.getAccessList.join('; ');

    };

    $scope.showStoreFilterOpt = function (val) {
        angular.element('.block-filter').hide();
        if (val == 'on') {
            angular.element('.filter-store').show();
        } else {
            angular.element('.filter-store').hide();
        }
    }

    $scope.getStoreLocList = $stateParams.StorageLocation ? $stateParams.StorageLocation.split("; ") : [];
    $scope.store_list = $scope.getStoreLocList.join('; ');

    $scope.storeLocFilter = function (value, checked) {

        var idx = $scope.getStoreLocList.indexOf(value);
        if (idx >= 0 && !checked) {
            $scope.getStoreLocList.splice(idx, 1);
        }
        if (idx < 0 && checked) {
            $scope.getStoreLocList.push(value);
        }
        $scope.store_list = $scope.getStoreLocList.join('; ');
    };

    $scope.clearFilter = function () {
        $scope.artist_list = null;
        $scope.author_list = null;
        $scope.filter_Subject = null;
        $scope.material_list = null;
        $scope.medium_list = null;
        $scope.location_list = null;
        $scope.access_list = null;
        $scope.store_list = null;
        $scope.display_list = null;
        $scope.filter_LibLanguage = null;
        $scope.filter_arcLanguage = null;
        $scope.country_list = null;
        $scope.collections_list = null;
        $scope.itemAvailability_list = null;
        $scope.imageAvailability_list = null;
        $scope.showANDlanguage_list = null;

        if ($scope.getArtistList.length > 0) {
            $scope.getArtistList.splice(0, $scope.getArtistList.length);
        }
        if ($scope.getAuthorList.length > 0) {
            $scope.getAuthorList.splice(0, $scope.getAuthorList.length);
        }
        if ($scope.getMaterialList.length > 0) {
            $scope.getMaterialList.splice(0, $scope.getMaterialList.length);
        }
        if ($scope.getMediumList.length > 0) {
            $scope.getMediumList.splice(0, $scope.getMediumList.length);
        }
        if ($scope.getLocList.length > 0) {
            $scope.getLocList.splice(0, $scope.getLocList.length);
        }
        if ($scope.getAccessList.length > 0) {
            $scope.getAccessList.splice(0, $scope.getAccessList.length);
        }
        if ($scope.getStoreLocList.length > 0) {
            $scope.getStoreLocList.splice(0, $scope.getStoreLocList.length);
        }
        if ($scope.getLibLangList.length > 0) {
            $scope.getLibLangList.splice(0, $scope.getLibLangList.length);
        }

        if ($scope.getArcLangList.length > 0) {
            $scope.getArcLangList.splice(0, $scope.getArcLangList.length);
        }
        if ($scope.getCountryList.length > 0) {
            $scope.getCountryList.splice(0, $scope.getCountryList.length);
        }
        if ($scope.getColList.length > 0) {
            $scope.getColList.splice(0, $scope.getColList.length);
        }
        if ($scope.getItemAvailabilityList.length > 0) {
            $scope.getItemAvailabilityList.splice(0, $scope.getItemAvailabilityList.length);
        }

        $scope.getDisplayList = null;

        //  $scope.getArtistList = [];
        //  $scope.getAuthorList = [];
        //  $scope.getAuthorList = [];
        // //$scope.getMaterialList = [];
        //  $scope.getMediumList = [];
        //  $scope.getLocList = [];
        //  $scope.getAccessList = [];
        //  $scope.getStoreLocList = [];
        //  $scope.getLibLangList = [];
        //  $scope.getArcLangList = [];
        //  $scope.getCountryList = [];
        //  $scope.getColList = [];
        //  $scope.getItemAvailabilityList = [];
        $scope.getImageAvailabilityList = [];

        $state.go("search", {
            'FilterArtist': null,
            'FilterLanguage': null,
            'FilterLocation': null,
            'FilterSubject': null,
            'ImageAvailability': null,
            'ItemAvailability': null,
            'FilterLanguageOperator': null,
            'FilterMedium': null,
            'Collection': null,
            'AccessLevel': null,
            'StorageLocation': null,
            'OnDisplay': null,
            'MaterialType': null,
            'MeetingName': null
        }, {
            notify: false
        });
        sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY] = JSON.stringify({
            'FilterArtist': null,
            'FilterLanguage': null,
            'FilterLocation': null,
            'FilterSubject': null,
            'ImageAvailability': null,
            'ItemAvailability': null,
            'FilterLanguageOperator': null,
            'FilterMedium': null,
            'Collection': null,
            'AccessLevel': null,
            'StorageLocation': null,
            'OnDisplay': null,
            'MaterialType': null
        });

        $scope.init(false, false);
    }

    $scope.AdvSearch = function () {
        angular.element(".search-dropdown").toggleClass("avtive");
        angular.element(".icon-advanceSearch").toggleClass("active");
        sessionStorage[RELATEDITEM_STORAGE_KEY] = false;
        $scope.loading = true;
        $scope.toggleValue = 'ALL';
        $scope.searchType = 'a';
        sessionStorage[FILTER_STORAGE_KEY] = 'a';

        $scope.genericSearch.Search = '';

        if ($scope.artTabActive) {
            sessionStorage[ADVSEARCHCATE_STORAGE_KEY] = 'ART';
            sessionStorage[JSON_ADVSEARCH_STORAGE_KEY] = JSON.stringify($scope.artFormData);
            $scope.toggleValue = 'ART';

            $state.go("search", {
                'Search': null,
                'SearchType': 'a',
                'CategoryCode': 'ART',
                'relatedSearch': null,
                'SourceTitle': $scope.artFormData.SourceTitle,
                'SourceNo': $scope.artFormData.SourceNo,
                'Artist': $scope.artFormData.Artist,
                'Creditline': $scope.artFormData.Creditline,
                'ProductDate': $scope.artFormData.ProductDate,
                'Medium': $scope.artFormData.Medium,
                'Location': $scope.artFormData.Location,
                'FilterArtist': null,
                'FilterLanguage': null,
                'FilterLocation': null,
                'FilterSubject': null,
                'ImageAvailability': null,
                'FilterLanguageOperator': null,
                'FilterMedium': null,
                'Collection': null,
                'AccessLevel': null,
                'StorageLocation': null,
                'OnDisplay': null,
                'MaterialType': null,
                'MeetingName': null
            }, {
                notify: false
            });


        } else if ($scope.libTabActive) {
            sessionStorage[ADVSEARCHCATE_STORAGE_KEY] = 'LIB';
            sessionStorage[JSON_ADVSEARCH_STORAGE_KEY] = JSON.stringify($scope.libFormData);

            $scope.toggleValue = 'LIB';
            $state.go("search", {
                'Search': null,
                'SearchType': 'a',
                'CategoryCode': 'LIB',
                'relatedSearch': null,
                'SourceTitle': $scope.libFormData.SourceTitle,
                'Artist': $scope.libFormData.Artist,
                'Location': $scope.libFormData.Location,
                'Subject': $scope.libFormData.Subject,
                'Language': $scope.libFormData.Language,
                'MeetingName': $scope.libFormData.MeetingName,
                'Imprint': $scope.libFormData.Imprint,
                'SeriesTitle': $scope.libFormData.SeriesTitle,
                'ISBN': $scope.libFormData.ISBN,
                'ISSN': $scope.libFormData.ISSN,
                'DeweyClass': $scope.libFormData.DeweyClass,
                'ProductDate': $scope.libFormData.ProductDate,
                'FilterArtist': null,
                'FilterLanguage': null,
                'FilterLocation': null,
                'FilterSubject': null,
                'ImageAvailability': null,
                'FilterLanguageOperator': null,
                'FilterMedium': null,
                'Collection': $scope.libFormData.Collection,
                'AccessLevel': null,
                'StorageLocation': null,
                'OnDisplay': null,
                'MaterialType': null
            }, {
                notify: false
            });


        } else if ($scope.arcTabActive) {
            sessionStorage[ADVSEARCHCATE_STORAGE_KEY] = 'ARC';
            sessionStorage[JSON_ADVSEARCH_STORAGE_KEY] = JSON.stringify($scope.arcFormData);

            $scope.toggleValue = 'ARC';
            $state.go("search", {
                'Search': null,
                'SearchType': 'a',
                'CategoryCode': 'ARC',
                'relatedSearch': null,
                'SourceTitle': $scope.arcFormData.SourceTitle,
                'Artist': $scope.arcFormData.Artist,
                'Subject': $scope.arcFormData.Subject,
                'Language': $scope.arcFormData.Language,
                'ProductDate': $scope.arcFormData.ProductDate,
                'FondsCollection': $scope.arcFormData.FondsCollection,
                'ScopeAndContent': $scope.arcFormData.ScopeAndContent,
                'FilterArtist': null,
                'FilterLanguage': null,
                'FilterLocation': null,
                'FilterSubject': null,
                'ImageAvailability': null,
                'FilterLanguageOperator': null,
                'FilterMedium': null,
                'Collection': null,
                'AccessLevel': null,
                'StorageLocation': null,
                'OnDisplay': null,
                'MaterialType': null
            }, {
                notify: false
            });

        }

        $scope.init(false, false);

    }

    $scope.basicSearch = function () {
        $scope.loading = true;
        $scope.searchType = 'b';
        $scope.isMoreResultForArt = true;
        $scope.isMoreResultForAll = true;
        $scope.isMoreResultForArc = true;
        $scope.isMoreResultForLib = true;
        sessionStorage[FILTER_STORAGE_KEY] = 'b';
        sessionStorage[JSON_BSSEARCH_STORAGE_KEY] = $scope.genericSearch.Search ? $scope.genericSearch.Search : '';
        sessionStorage[CATE_SEARCH_STORAGE_KEY] = null;
        sessionStorage[RELATEDITEM_STORAGE_KEY] = false;
        angular.element('.bs_autocomplete').hide();


        $scope.artist_list = null;
        $scope.author_list = null;
        $scope.filter_Subject = null;
        $scope.material_list = null;
        $scope.medium_list = null;
        $scope.location_list = null;
        $scope.access_list = null;
        $scope.store_list = null;
        $scope.display_list = null;
        $scope.filter_LibLanguage = null;
        $scope.filter_arcLanguage = null;
        $scope.country_list = null;
        $scope.collections_list = null;
        $scope.itemAvailability_list = null;
        $scope.imageAvailability_list = null;
        $scope.showANDlanguage_list = null;

        if ($scope.getArtistList.length > 0) {
            $scope.getArtistList.splice(0, $scope.getArtistList.length);
        }
        if ($scope.getAuthorList.length > 0) {
            $scope.getAuthorList.splice(0, $scope.getAuthorList.length);
        }
        if ($scope.getMaterialList.length > 0) {
            $scope.getMaterialList.splice(0, $scope.getMaterialList.length);
        }
        if ($scope.getMediumList.length > 0) {
            $scope.getMediumList.splice(0, $scope.getMediumList.length);
        }
        if ($scope.getLocList.length > 0) {
            $scope.getLocList.splice(0, $scope.getLocList.length);
        }
        if ($scope.getAccessList.length > 0) {
            $scope.getAccessList.splice(0, $scope.getAccessList.length);
        }
        if ($scope.getStoreLocList.length > 0) {
            $scope.getStoreLocList.splice(0, $scope.getStoreLocList.length);
        }
        if ($scope.getLibLangList.length > 0) {
            $scope.getLibLangList.splice(0, $scope.getLibLangList.length);
        }

        if ($scope.getArcLangList.length > 0) {
            $scope.getArcLangList.splice(0, $scope.getArcLangList.length);
        }
        if ($scope.getCountryList.length > 0) {
            $scope.getCountryList.splice(0, $scope.getCountryList.length);
        }
        if ($scope.getColList.length > 0) {
            $scope.getColList.splice(0, $scope.getColList.length);
        }
        if ($scope.getItemAvailabilityList.length > 0) {
            $scope.getItemAvailabilityList.splice(0, $scope.getItemAvailabilityList.length);
        }

        $scope.getDisplayList = null;

        //  $scope.getArtistList = [];
        //  $scope.getAuthorList = [];
        //  $scope.getAuthorList = [];
        // //$scope.getMaterialList = [];
        //  $scope.getMediumList = [];
        //  $scope.getLocList = [];
        //  $scope.getAccessList = [];
        //  $scope.getStoreLocList = [];
        //  $scope.getLibLangList = [];
        //  $scope.getArcLangList = [];
        //  $scope.getCountryList = [];
        //  $scope.getColList = [];
        //  $scope.getItemAvailabilityList = [];
        $scope.getImageAvailabilityList = [];

        sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY] = JSON.stringify({
            'FilterArtist': null,
            'FilterLanguage': null,
            'FilterLocation': null,
            'FilterSubject': null,
            'ImageAvailability': null,
            'ItemAvailability': null,
            'FilterLanguageOperator': null,
            'FilterMedium': null,
            'Collection': null,
            'AccessLevel': null,
            'StorageLocation': null,
            'OnDisplay': null,
            'MaterialType': null
        });

        $state.go("search", {
            'Search': $scope.genericSearch.Search ? $scope.genericSearch.Search : null,
            'SearchType': 'b',
            'relatedSearch': null,
            'CategoryCode': $scope.toggleValue == 'ALL' ? '' : $scope.toggleValue,
            'SourceTitle': null,
            'SourceNo': null,
            'Artist': null,
            'Creditline': null,
            'ProductDate': null,
            'Medium': null,
            'FondsCollection': null,
            'ScopeAndContent': null,
            'Location': null,
            'Subject': null,
            'Language': null,
            'MeetingName': null,
            'Imprint': null,
            'SeriesTitle': null,
            'ISBN': null,
            'ISSN': null,
            'DeweyClass': null,
            'FilterArtist': null,
            'FilterLanguage': null,
            'FilterLocation': null,
            'FilterSubject': null,
            'ImageAvailability': null,
            'FilterLanguageOperator': null,
            'FilterMedium': null,
            'Collection': null,
            'AccessLevel': null,
            'StorageLocation': null,
            'OnDisplay': null,
            'MaterialType': null,
        }, {
            notify: false
        });

        $scope.init(false, false);
    }

    $('#searchPage').on('keydown', function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            if (!angular.element(".search-dropdown").hasClass("avtive")) {
                $scope.loading = true;
                $scope.searchType = 'b';
                $scope.isMoreResultForArt = true;
                $scope.isMoreResultForAll = true;
                $scope.isMoreResultForArc = true;
                $scope.isMoreResultForLib = true;
                sessionStorage[FILTER_STORAGE_KEY] = 'b';
                sessionStorage[JSON_BSSEARCH_STORAGE_KEY] = $scope.genericSearch.Search ? $scope.genericSearch.Search : '';
                sessionStorage[CATE_SEARCH_STORAGE_KEY] = null;
                sessionStorage[RELATEDITEM_STORAGE_KEY] = false;
                angular.element('.bs_autocomplete').hide();

                $scope.artist_list = null;
                $scope.author_list = null;
                $scope.filter_Subject = null;
                $scope.material_list = null;
                $scope.medium_list = null;
                $scope.location_list = null;
                $scope.access_list = null;
                $scope.store_list = null;
                $scope.display_list = null;
                $scope.filter_LibLanguage = null;
                $scope.filter_arcLanguage = null;
                $scope.country_list = null;
                $scope.collections_list = null;
                $scope.itemAvailability_list = null;
                $scope.imageAvailability_list = null;
                $scope.showANDlanguage_list = null;

                if ($scope.getArtistList.length > 0) {
                    $scope.getArtistList.splice(0, $scope.getArtistList.length);
                }
                if ($scope.getAuthorList.length > 0) {
                    $scope.getAuthorList.splice(0, $scope.getAuthorList.length);
                }
                if ($scope.getMaterialList.length > 0) {
                    $scope.getMaterialList.splice(0, $scope.getMaterialList.length);
                }
                if ($scope.getMediumList.length > 0) {
                    $scope.getMediumList.splice(0, $scope.getMediumList.length);
                }
                if ($scope.getLocList.length > 0) {
                    $scope.getLocList.splice(0, $scope.getLocList.length);
                }
                if ($scope.getAccessList.length > 0) {
                    $scope.getAccessList.splice(0, $scope.getAccessList.length);
                }
                if ($scope.getStoreLocList.length > 0) {
                    $scope.getStoreLocList.splice(0, $scope.getStoreLocList.length);
                }
                if ($scope.getLibLangList.length > 0) {
                    $scope.getLibLangList.splice(0, $scope.getLibLangList.length);
                }

                if ($scope.getArcLangList.length > 0) {
                    $scope.getArcLangList.splice(0, $scope.getArcLangList.length);
                }
                if ($scope.getCountryList.length > 0) {
                    $scope.getCountryList.splice(0, $scope.getCountryList.length);
                }
                if ($scope.getColList.length > 0) {
                    $scope.getColList.splice(0, $scope.getColList.length);
                }
                if ($scope.getItemAvailabilityList.length > 0) {
                    $scope.getItemAvailabilityList.splice(0, $scope.getItemAvailabilityList.length);
                }

                $scope.getDisplayList = null;

                //  $scope.getArtistList = [];
                //  $scope.getAuthorList = [];
                //  $scope.getAuthorList = [];
                // //$scope.getMaterialList = [];
                //  $scope.getMediumList = [];
                //  $scope.getLocList = [];
                //  $scope.getAccessList = [];
                //  $scope.getStoreLocList = [];
                //  $scope.getLibLangList = [];
                //  $scope.getArcLangList = [];
                //  $scope.getCountryList = [];
                //  $scope.getColList = [];
                //  $scope.getItemAvailabilityList = [];
                $scope.getImageAvailabilityList = [];

                sessionStorage[JSON_FILTERSEARCH_STORAGE_KEY] = JSON.stringify({
                    'FilterArtist': null,
                    'FilterLanguage': null,
                    'FilterLocation': null,
                    'FilterSubject': null,
                    'ImageAvailability': null,
                    'FilterLanguageOperator': null,
                    'ItemAvailability': null,
                    'FilterMedium': null,
                    'Collection': null,
                    'AccessLevel': null,
                    'StorageLocation': null,
                    'OnDisplay': null,
                    'MaterialType': null
                });

                $state.go("search", {
                    'Search': $scope.genericSearch.Search ? $scope.genericSearch.Search : null,
                    'SearchType': 'b',
                    'relatedSearch': null,
                    'CategoryCode': $scope.toggleValue == 'ALL' ? '' : $scope.toggleValue,
                    'SourceTitle': null,
                    'SourceNo': null,
                    'Artist': null,
                    'Creditline': null,
                    'ProductDate': null,
                    'Medium': null,
                    'FondsCollection': null,
                    'ScopeAndContent': null,
                    'Location': null,
                    'Subject': null,
                    'Language': null,
                    'MeetingName': null,
                    'Imprint': null,
                    'SeriesTitle': null,
                    'ISBN': null,
                    'ISSN': null,
                    'DeweyClass': null,
                    'FilterArtist': null,
                    'FilterLanguage': null,
                    'FilterLocation': null,
                    'FilterSubject': null,
                    'ImageAvailability': null,
                    'FilterLanguageOperator': null,
                    'FilterMedium': null,
                    'Collection': null,
                    'AccessLevel': null,
                    'StorageLocation': null,
                    'OnDisplay': null,
                    'MaterialType': null,
                }, {
                    notify: false
                });

                $scope.init(false, false);
            } else {
                $scope.AdvSearch();
            }
        }
    });

    $scope.focusedIndex = 0;

    $scope.handleKeyDown = function ($event) {
        var keyCode = $event.keyCode;

        if (keyCode === 40) {
            // Down
            $event.preventDefault();
            if ($scope.focusedIndex !== $scope.autoCompleteLength - 1) {
                $scope.focusedIndex++;
                $scope.genericSearch.Search = angular.element('.autocompleteResults li').eq($scope.focusedIndex).text();
            }
        } else if (keyCode === 38) {
            // Up
            $event.preventDefault();
            if ($scope.focusedIndex !== 0) {
                $scope.focusedIndex--;
                $scope.genericSearch.Search = angular.element('.autocompleteResults li').eq($scope.focusedIndex).text();
            }
        }

    };

    $scope.advanceSearch3 = function () {
        angular.element('.advance-search-3').toggle('fast', 'linear');
    }

    $scope.sorting = function () {
        $scope.loading = true;
        $scope.isSort = $scope.isSort ? false : true;
        $scope.init(false, false);
    }

    $scope.gridView = function () {
        $scope.loading = true;
        $scope.isGridView = true;
        $scope.glViewArt = true;
        $scope.glViewArc = true;
        $scope.glViewLib = true;
        $scope.glViewOther = true;
        // if ($scope.toggleValue == 'ART') {
        //     $scope.glViewArt = true;
        // } else if ($scope.toggleValue == 'ARC') {
        //     $scope.glViewArc = true;
        // } else if ($scope.toggleValue == 'LIB') {
        //     $scope.glViewLib = true;
        // } else if ($scope.toggleValue == 'ALL') {
        //     $scope.glViewOther = true;
        // }
        $scope.init(false, false);

    }
    $scope.listView = function () {
        $scope.loading = true;
        $scope.isGridView = false;
        $scope.glViewArt = false;
        $scope.glViewArc = false;
        $scope.glViewLib = false;
        $scope.glViewOther = false;
        // if ($scope.toggleValue == 'ART') {
        //     $scope.glViewArt = false;
        // } else if ($scope.toggleValue == 'ARC') {
        //     $scope.glViewArc = false;
        // } else if ($scope.toggleValue == 'LIB') {
        //     $scope.glViewLib = false;
        // } else if ($scope.toggleValue == 'ALL') {
        //     $scope.glViewOther = false;
        // }
        $scope.init(false, false);
        // $scope.loading = true;
    }

    function getProduct(filter) {
        sessionStorage[ITEM2DETAILS_STORAGE_KEY] = JSON.stringify(filter);
        $scope.loading = true;
        $scope.noResult = false;

        if ($scope.toggleValue == 'ALL') {
            ApiService.getproduct(filter)
                .then(function (r) {
                    if (r.data.Status === 'OK') {
                        var thumb = '';
                        $scope.data = r.data.Products;


                        // if($scope.genericSearch.Search != ''){
                        //     $scope.data = r.data.Products;

                        // }else{
                        //     $scope.allTabFirstTimeLoad = false;
                        //      if($scope.allTabCategory == "Art" && r.data.Art.length > 0){
                        //         console.log('Art');
                        //         $scope.data = r.data.Art;
                        //     }else if($scope.allTabCategory == "Arc" &&  r.data.Arc.length > 0){
                        //          console.log('Arc');
                        //          $scope.data = r.data.Arc;
                        //     }else if($scope.allTabCategory == "Lib" && r.data.Lib.length > 0){
                        //         console.log('Lib');
                        //          $scope.data = r.data.Lib;
                        //     }else if(r.data.Art.length <= 0 && r.data.Arc.length <= 0 && r.data.Lib.length <= 0){
                        //         console.log('End');
                        //          $scope.loading = false;
                        //          $scope.isMoreResultForAll = false;
                        //          return;
                        //     }else{
                        //         console.log('Continue');
                        //         $scope.loadMore();
                        //     }
                        // }


                        //$scope.data = r.data.Products;
                        //$scope.allGridData.push(r.data.Products);
                        try {
                            $scope.data.forEach(function (item) {
                                $scope.allGridData.push(item);
                            });
                            // if($scope.genericSearch.Search != ''){
                            //    $scope.data.forEach(function(item) {
                            //        $scope.allGridData.push(item);
                            //    });
                            // }else{
                            //    $scope.allGridData = [].concat($scope.allGridData,$scope.data);
                            // }
                            // 

                            $allGrid.imagesLoaded({
                                background: true
                            }).fail(function () {
                                $scope.loading = false;
                                $scope.loadPage = true;
                                $timeout(function () {
                                    $scope.imagesPreload = false;
                                }, 500);
                            }).done(function () {
                                $scope.loading = false;
                                $scope.loadPage = true;
                                $timeout(function () {
                                    $scope.imagesPreload = false;
                                }, 500);
                            });



                        } catch (ex) {
                            $scope.isMoreResultForAll = false;
                            $scope.loading = false;
                            //$scope.noResult = true;
                            //$scope.AdvanceSearch.PageNumber = 1;
                        }
                    } else {
                        $scope.isMoreResultForAll = false;
                        $scope.loading = false;
                        //$scope.noResult = true;
                    }

                });
        } else if ($scope.toggleValue == 'ART') {
            ApiService.getproduct(filter)
                .then(function (r) {
                    if (r.data.Status === 'OK' && (sessionStorage[ADVSEARCHCATE_STORAGE_KEY] == 'ART' || sessionStorage[FILTER_STORAGE_KEY] == 'b')) {
                        var thumb = '';
                        $scope.data = r.data.Products;
                        //$scope.allGridData.push(r.data.Products);
                        try {
                            $scope.data.forEach(function (item) {
                                $scope.artGridData.push(item);

                            });
                            $artGrid.imagesLoaded({
                                background: true
                            }).fail(function () {
                                $scope.loading = false;
                                $scope.loadPage = true;
                                $timeout(function () {
                                    $scope.imagesPreload = false;
                                }, 500);
                            }).done(function () {
                                $scope.loading = false;
                                $scope.loadPage = true;
                                $timeout(function () {
                                    $scope.imagesPreload = false;
                                }, 500);
                            });

                        } catch (ex) {
                            $scope.isMoreResultForArt = false;
                            $scope.loading = false;
                            //$scope.noResult = true;
                            //$scope.AdvanceSearch.PageNumber = 1;
                        }
                    } else {
                        $scope.isMoreResultForArt = false;
                        $scope.loading = false;
                        //$scope.noResult = true;
                    }

                });
        } else if ($scope.toggleValue == 'ARC') {
            ApiService.getproduct(filter)
                .then(function (r) {
                    if (r.data.Status === 'OK' && (sessionStorage[ADVSEARCHCATE_STORAGE_KEY] == 'ARC' || sessionStorage[FILTER_STORAGE_KEY] == 'b')) {
                        var thumb = '';
                        $scope.data = r.data.Products;
                        //$scope.allGridData.push(r.data.Products);
                        try {
                            $scope.data.forEach(function (item) {
                                $scope.arcGridData.push(item);
                            });

                            $arcGrid.imagesLoaded({
                                background: true
                            }).fail(function () {
                                $scope.loading = false;
                                $scope.loadPage = true;
                                $timeout(function () {
                                    $scope.imagesPreload = false;
                                }, 500);
                            }).done(function () {
                                $scope.loading = false;
                                $scope.loadPage = true;
                                $timeout(function () {
                                    $scope.imagesPreload = false;
                                }, 500);
                            });


                        } catch (ex) {
                            $scope.isMoreResultForArc = false;
                            $scope.loading = false;
                            //$scope.noResult = true;
                            //$scope.AdvanceSearch.PageNumber = 1;
                        }
                    } else {
                        $scope.isMoreResultForArc = false;
                        $scope.loading = false;
                        //$scope.noResult = true;
                    }

                });
        } else if ($scope.toggleValue == 'LIB') {
            ApiService.getproduct(filter)
                .then(function (r) {
                    if (r.data.Status === 'OK' && (sessionStorage[ADVSEARCHCATE_STORAGE_KEY] == 'LIB' || sessionStorage[FILTER_STORAGE_KEY] == 'b')) {
                        var thumb = '';
                        $scope.data = r.data.Products;
                        //$scope.allGridData.push(r.data.Products);
                        try {

                            $scope.data.forEach(function (item) {
                                $scope.libGridData.push(item);
                            });
                            $libGrid.imagesLoaded({
                                background: true
                            }).fail(function () {
                                $scope.loading = false;
                                $scope.loadPage = true;
                                $timeout(function () {
                                    $scope.imagesPreload = false;
                                }, 500);
                            }).done(function () {
                                $scope.loading = false;
                                $scope.loadPage = true;
                                $timeout(function () {
                                    $scope.imagesPreload = false;
                                }, 500);
                            });


                        } catch (ex) {
                            $scope.isMoreResultForLib = false;
                            $scope.loading = false;
                            //$scope.noResult = true;
                            //$scope.AdvanceSearch.PageNumber = 1;
                        }
                    } else {
                        $scope.isMoreResultForLib = false;
                        $scope.loading = false;
                        //$scope.noResult = true;
                    }

                });
        }


    }

    $scope.loadMore = function () {

        if ($scope.loadPage) {
            if ($scope.toggleValue == 'ALL' && $scope.isMoreResultForAll) {

                $scope.loading = true;
                $scope.pageNumber = parseInt(sessionStorage[ALLPAGE_STORAGE_KEY]);

                // var cur_val = $scope.pageNumber;
                // var new_val = cur_val + 1;
                if ($scope.isSort) {
                    sortVal = "TITLE";
                } else {
                    sortVal = "";
                }
                isBasicSearch = $scope.searchType ? $scope.searchType : $stateParams.SearchType;
                $scope.AdvanceSearch = $scope.searchData($scope.pageNumber, isBasicSearch, sortVal, 'ALL');

                // if($scope.allTabCategory == "Art"){
                //     $scope.allTabCategory = "Arc";
                // }else if($scope.allTabCategory == "Arc"){
                //     $scope.allTabCategory = "Lib";
                // }else if($scope.allTabCategory == "Lib"){
                //     $scope.allTabCategory = "Art";
                // }

                // if($scope.libLength <= 0){
                //     if($scope.allTabCategory == "Arc" && !$scope.allTabFirstTimeLoad){
                //         $scope.pageNumber = $scope.pageNumber + 1;
                //     }
                // }else if($scope.arcLength <= 0){
                //     if($scope.allTabCategory == "Art" && !$scope.allTabFirstTimeLoad){
                //         $scope.pageNumber = $scope.pageNumber + 1;
                //     }
                // }else if($scope.artLength <= 0){
                //     if($scope.allTabCategory == "Lib" && !$scope.allTabFirstTimeLoad){
                //         $scope.pageNumber = $scope.pageNumber + 1;
                //     }
                // }

                // if(($scope.allTabCategory == "Lib" && !$scope.allTabFirstTimeLoad && $scope.artLength > 0) || ($scope.artLength > 0 && $scope.arcLength <= 0 && $scope.libLength <= 0) || ($scope.artLength <= 0 && $scope.arcLength > 0 && $scope.libLength <= 0) || ($scope.artLength <= 0 && $scope.arcLength <= 0 && $scope.libLength > 0)){
                //     $scope.pageNumber = $scope.pageNumber + 1;
                // }
                // if($scope.genericSearch.Search != ''){
                //      $scope.pageNumber = $scope.pageNumber + 1;
                // }else{
                //     if($scope.allTabCategory == "Lib" && !$scope.allTabFirstTimeLoad){
                //         $scope.pageNumber = $scope.pageNumber + 1;
                //     }
                // }

                getProduct($scope.AdvanceSearch);
                $scope.pageNumber = $scope.pageNumber + 1;

                sessionStorage[ALLPAGE_STORAGE_KEY] = $scope.pageNumber;

            } else if ($scope.toggleValue == 'ART' && $scope.isMoreResultForArt) {

                $scope.loading = true;
                $scope.pageNumber = parseInt(sessionStorage[ARTPAGE_STORAGE_KEY]);
                // var cur_val = $scope.pageNumber;
                // var new_val = cur_val + 1;
                if ($scope.isSort) {
                    sortVal = "TITLE";
                } else {
                    sortVal = "";
                }
                isBasicSearch = $scope.searchType ? $scope.searchType : $stateParams.SearchType;
                $scope.AdvanceSearch = $scope.searchData($scope.pageNumber, isBasicSearch, sortVal, 'ART');

                getProduct($scope.AdvanceSearch);
                $scope.pageNumber = $scope.pageNumber + 1;

                sessionStorage[ARTPAGE_STORAGE_KEY] = $scope.pageNumber;

            } else if ($scope.toggleValue == 'ARC' && $scope.isMoreResultForArc) {

                $scope.loading = true;
                $scope.pageNumber = parseInt(sessionStorage[ARCPAGE_STORAGE_KEY]);
                // var cur_val = $scope.pageNumber;
                // var new_val = cur_val + 1;
                if ($scope.isSort) {
                    sortVal = "TITLE";
                } else {
                    sortVal = "";
                }
                isBasicSearch = $scope.searchType ? $scope.searchType : $stateParams.SearchType;
                $scope.AdvanceSearch = $scope.searchData($scope.pageNumber, isBasicSearch, sortVal, 'ARC');

                getProduct($scope.AdvanceSearch);
                $scope.pageNumber = $scope.pageNumber + 1;

                sessionStorage[ARCPAGE_STORAGE_KEY] = $scope.pageNumber;

            } else if ($scope.toggleValue == 'LIB' && $scope.isMoreResultForLib) {

                $scope.loading = true;
                $scope.pageNumber = parseInt(sessionStorage[LIBPAGE_STORAGE_KEY]);
                // var cur_val = $scope.pageNumber;
                // var new_val = cur_val + 1;
                if ($scope.isSort) {
                    sortVal = "TITLE";
                } else {
                    sortVal = "";
                }
                isBasicSearch = $scope.searchType ? $scope.searchType : $stateParams.SearchType;
                $scope.AdvanceSearch = $scope.searchData($scope.pageNumber, isBasicSearch, sortVal, 'LIB');

                getProduct($scope.AdvanceSearch);
                $scope.pageNumber = $scope.pageNumber + 1;

                sessionStorage[LIBPAGE_STORAGE_KEY] = $scope.pageNumber;
            }
            $scope.loadPage = false;
        }

    }

    $scope.autoAdvArtistComplete = function (keyword) {
        $scope.keywords = {
            Search: keyword,
            ItemCount: 5
        }

        $timeout(function () {
            ApiService.GetArtistAutoComplete($scope.keywords).then(function (r) {
                angular.element('.bs_AdvOptionsList').show();
                $scope.autoAdvArtistData = r.data;
            });
        }, 300);

    }

    $scope.clickToAdvAuthorSearch = function (keyword) {
        angular.element('.bs_AdvOptionsList').hide();
        $scope.libFormData.Artist = keyword;
    }

    $scope.autoAdvAuthorComplete = function (keyword) {
        $scope.keywords = {
            Search: keyword,
            ItemCount: 5
        }

        $timeout(function () {
            ApiService.GetArtistAutoComplete($scope.keywords).then(function (r) {
                angular.element('.bs_AdvOptionsList').show();
                $scope.autoAdvAuthorData = r.data;
            });
        }, 300);

    }

    $scope.clickToAdvArtistSearch = function (keyword) {
        angular.element('.bs_AdvOptionsList').hide();
        $scope.artFormData.Artist = keyword;
    }

    $scope.open_advanceSearch = function () {
        angular.element(".search-dropdown").toggleClass("avtive");
        angular.element(".icon-advanceSearch").toggleClass("active");
    }

    $scope.triggerFilterBlock = function () {
        var filterBlock = angular.element('#filter-block');
        filterBlock.toggleClass('active');
    }

    $scope.collectionSelection = function () {
        $timeout(function () {
            angular.element('.bs_AdvOptionsCollectionList').show();
        }, 300);
    }

    $scope.clickToCollectionField = function (val) {
        $scope.libFormData.Collection = val;
        angular.element('.bs_AdvOptionsCollectionList').hide();
    }

    $scope.clickViewItem = function (productId) {
        if ($scope.keyWord != null && $scope.keyWord != '') {
            if (req.CategoryCode != null && req.CategoryCode.Length > 0) {
                keyWord = keyWord + "CategoryCode: " + string.Join(",", req.CategoryCode) + ";";
            }
            keyWord = keyWord.Substring(0, keyWord.Length - 1);

            $scope.requestedData = {
                Keyword: $scope.keyWord,
                productId: productId,
            }

            ApiService.trackItemClick($scope.requestedData)
                .then(function (r) {
                    if (r.statusText === 'OK') {
                        $scope.loading = false;
                        $window.location.href = '/details/Search/' + productId.toString();
                        // $state.go('success', {
                        //     id: r.data.Request.RequestId
                        // });
                    } else {
                        // $window.alert('Form Submission Failed');
                        console.error("Track item click ")
                    }

                });
        }
    }

}]);

app.controller('detailsCtrl', ['$scope', '$location', '$http', '$interval', '$mdDialog', 'ApiService', '$stateParams', '$timeout', '$window', '$state', '$sce', '$interval', function ($scope, $location, $http, $interval, $mdDialog, ApiService, $stateParams, $timeout, $window, $state, $sce, $interval) {
    $scope.$parent.location = 'details';
    $scope.$parent.homeTop = true;
    $scope.$parent.hasOptionBar = false;
    $scope.from = $stateParams.from;
    $scope.SearchCategory = sessionStorage[ADVSEARCHCATE_STORAGE_KEY];
    $scope.searchTypeVal = sessionStorage[FILTER_STORAGE_KEY];

    $scope.bsSearchVal = sessionStorage[JSON_BSSEARCH_STORAGE_KEY];
    $scope.advSearchVal = sessionStorage[ITEM2DETAILS_STORAGE_KEY] ? JSON.parse(sessionStorage[ITEM2DETAILS_STORAGE_KEY]) : {};
    $scope.curUrl = $location.absUrl();
    $scope.cover = true;
    var poster = '';

    var productId = $stateParams.productId;
    var proBrn = $stateParams.BRN;
    var imagetoDialog = '';
    var titletoDialog = '';

    if(DOWNTIME){
        $state.go('downtime');
    }

    $scope.artFormData = {
        Artist: '',
        ArtistDateOfBirth: '',
        ArtistDateOfDeath: '',
        SourceTitle: '',
        SourceNo: '',
        ProductDate: '',
        Medium: '',
        Creditline: '',
        Location: ''
    }

    $scope.libFormData = {
        SourceTitle: '',
        Artist: '',
        ProductDate: '',
        Imprint: '',
        Location: '',
        Language: '',
        MeetingName: '',
        SeriesTitle: '',
        ISBN: '',
        ISSN: '',
        DeweyClass: '',

    }

    $scope.arcFormData = {
        SourceTitle: '',
        Artist: '',
        SourceNo: '',
        Subject: '',
        Language: '',
        ProductDate: '',
        FondsCollection: '',
        ScopeAndContent: ''
    }

    if ($scope.from == 'Search') {
        if ($scope.searchTypeVal == 'a') {
            $scope.advSearchVal['ProductId'] = productId;
            $scope.advSearchVal['BRN'] = proBrn;
            $scope.advSearchVal['PageSize'] = 1;
            $scope.details = $scope.advSearchVal;

            if ($scope.SearchCategory == 'LIB') {
                $scope.libFormData = JSON.parse(sessionStorage[JSON_ADVSEARCH_STORAGE_KEY]);
                // $scope.libFormData = {
                //     SourceTitle: $scope.advSearchVal.SourceTitle,
                //     Artist: $scope.advSearchVal.Artist,
                //     ProductDate: $scope.advSearchVal.ProductDate,
                //     Imprint: $scope.advSearchVal.Imprint,
                //     Location: $scope.advSearchVal.Location,
                //     Language: $scope.advSearchVal.Language,
                //     //MeetingName: $scope.advSearchVal.MeetingName,
                //     SeriesTitle: $scope.advSearchVal.SeriesTitle,
                //     ISBN: $scope.advSearchVal.ISBN,
                //     ISSN: $scope.advSearchVal.ISSN,
                //     DeweyClass: $scope.advSearchVal.DeweyClass,
                //     Subject: $scope.advSearchVal.Subject,
                //     Collection: $scope.advSearchVal.Collection,
                // }
            } else if ($scope.SearchCategory == 'ART') {
                $scope.artFormData = JSON.parse(sessionStorage[JSON_ADVSEARCH_STORAGE_KEY]);
                // $scope.artFormData = {
                //     Artist: $scope.advSearchVal.Artist,
                //     SourceTitle: $scope.advSearchVal.SourceTitle,
                //     Medium: $scope.advSearchVal.Medium,
                //     Creditline: $scope.artFormData.Creditline,
                //     SourceNo: $scope.advSearchVal.SourceNo,
                //     ProductDate: $scope.advSearchVal.ProductDate,
                //     Location: $scope.advSearchVal.Location
                // }
            } else if ($scope.SearchCategory == 'ARC') {
                $scope.arcFormData = JSON.parse(sessionStorage[JSON_ADVSEARCH_STORAGE_KEY]);
                // $scope.arcFormData = {
                //     SourceTitle: $scope.advSearchVal.SourceTitle,
                //     Artist: $scope.advSearchVal.Artist,
                //     SourceNo: $scope.advSearchVal.SourceNo,
                //     Subject: $scope.advSearchVal.Subject,
                //     Language: $scope.advSearchVal.Language,
                //     ProductDate: $scope.advSearchVal.ProductDate,
                //     FondsCollection: $scope.advSearchVal.FondsCollection,
                //     ScopeAndContent: $scope.advSearchVal.ScopeAndContent
                // }
            }

        } else {
            $scope.details = {
                ProductId: productId,
                BasicSearch: true,
                Search: $scope.bsSearchVal,
                BRN: proBrn
            }
        }


    } else {
        $scope.details = {
            ProductId: productId,
            BRN: proBrn
        }

    }

    $scope.genericSearch = {
        Search: $scope.bsSearchVal
    }

    $scope.colorCodes = ['#FFB5E8', '#FF9CEE', '#FFCCF9', '#FCC2FF', '#F6A6FF', '#B28DFF', '#C5A3FF', '#D5AAFF', '#ECD4FF', '#FBE4FF', '#DCD3FF', '#A79AFF', '#B5B9FF', '#97A2FF', '#AFCBFF', '#AFF8DB', '#C4FAF8', '#85E3FF', '#ACE7FF', '#6EB5FF', '#BFFCC6', '#DBFFD6', '#F3FFE3', '#E7FFAC', '#FFFFD1', '#FFC9DE', '#FFABAB', '#FFBEBC', '#FFCBC1', '#FFF5BA'];
    $scope.randomColor = $scope.colorCodes[Math.floor(Math.random() * $scope.colorCodes.length)];
    $scope.cate = '';
    // $scope.trustSrc = function(src) {
    //     return $sce.trustAsResourceUrl(src);
    // }
    ApiService.getproduct($scope.details)
        .then(function (r) {
            if (r.data.Status === 'OK') {
                $scope.loading = true;
                $scope.product = r.data.Products;
                $scope.relatedProducts = r.data.RelatedProducts;
                $scope.product.forEach(function (p) {
                    //if (p.ProductId == productId) {
                    $scope.item = p;
                    $scope.Credits = p.Credits ? p.Credits.replace(/\|\|/g, "<br />") : '';
                    //$scope.Notes = p.Notes ? p.Notes.replace(/\|\|/g, "<br />") : '';

                    $scope.Contents = p.Contents ? p.Contents.replace(/\|\|/g, "<br />") : '';
                    // $scope.Subject = p.Subject ? p.Subject.replace(/\|\|/g, "<br />") : '';
                    imagetoDialog = p.FilePath;
                    titletoDialog = p.SourceTitle;
                    imageCategoryCode = p.CategoryCode;
                    // if (p.CategoryCode == 'ART') {
                    //     $scope.cate = 'ARTWORK';
                    // } else if (p.CategoryCode == 'ARC') {
                    //     $scope.cate = 'Archives';
                    // } else {
                    //     $scope.cate = 'LIBRARY';
                    // }
                    thisImage = p.ImagePath;
                    filePath = p.FilePath;
                    fileFormat = p.FileFormat;

                    if (fileFormat == '.pdf') {
                        $scope.pdfUrl = $sce.trustAsResourceUrl('https://docs.google.com/viewer?url=' + $scope.imagePath + 'asset/Archives/' + filePath + '&embedded=true');
                        // $scope.pdfUrl = $sce.trustAsResourceUrl('/DocViewer/DocViewer/index?url=' + $scope.imagePath + 'asset/Archives/' + filePath);
                    } else if (fileFormat == '.mp4') {
                        $scope.pdfUrl = $sce.trustAsResourceUrl($scope.imagePath + 'asset/Archives/' + filePath);
                    }

                    // function dataURLtoFile(dataurl, filename) {

                    //     var arr = dataurl.split(','),
                    //         mime = arr[0].match(/:(.*?);/)[1],
                    //         bstr = atob(arr[1]), 
                    //         n = bstr.length, 
                    //         u8arr = new Uint8Array(n);

                    //     while(n--){
                    //         u8arr[n] = bstr.charCodeAt(n);
                    //     }

                    //     return new File([u8arr], filename, {type:mime});
                    // }

                    // $scope.url = {
                    //     url: $scope.imagePath + 'asset/Archives/' + filePath
                    // }

                    // ApiService.GetFile($scope.url).then(function(r) {
                    //     console.log(r.data);
                    //    var file = dataURLtoFile('data:text/html;base64,aGVsbG8gd29ybGQ=', r.data);
                    //    console.log(file.name);
                    //    $scope.pdfUrl = $sce.trustAsResourceUrl(file.name);
                    // });
                    // if (p.ProductId == 344) {
                    //     if ($(window).width() <= 767.98) {
                    //         $scope.pdfUrl = $sce.trustAsResourceUrl('https://docs.google.com/viewer?url=https://ngssource.s3-ap-southeast-1.amazonaws.com/testpdf/RC-I13-MS3-1_gsonly.pdf&embedded=true');
                    //     } else {
                    //         $scope.pdfUrl = $sce.trustAsResourceUrl('https://ngssource.s3-ap-southeast-1.amazonaws.com/testpdf/RC-I13-MS3-1_gsonly.pdf#toolbar=0');
                    //     }
                    // } else {
                    //     $scope.pdfUrl = $sce.trustAsResourceUrl('https://docs.google.com/viewer?url=' + $scope.imagePath + 'asset/Archives/' + filePath + '&embedded=true');
                    // }

                    // poster = $scope.imagePath + 'asset/Library/notavailable.png'; 

                    // controller.config = {
                    //     preload: "none",
                    //     autoHide: false,
                    //     autoHideTime: 3000,
                    //     autoPlay: false,
                    //     sources: controller.videos[0].sources,
                    //     theme: {
                    //         url: "css/videogular.css"
                    //     },
                    //     // plugins: {
                    //     //     poster: poster
                    //     // }
                    // };

                });

                //verify product has item
                if ($scope.details && $scope.details.ProductId && $scope.details.ProductId != '') {
                    if ($scope.item.CategoryCode == "LIB") {
                        if (!$scope.item.Items || $scope.item.Items.length == 0) {
                            $state.go('home');
                        }
                    }
                }

            } else {
                $state.go('home');

            }
            $scope.loading = true;

        });

    ApiService.getproduct({
        ProductId: productId,
        BRN: proBrn
    })
        .then(function (r) {
            if (r.data.Status === 'OK') {
                $scope.productList = r.data.Products;
                $scope.productList.forEach(function (p) {
                    $scope.productLink = p;
                });
            }
        });

    // $scope.openPDF = function() {
    //     window.open($scope.pdfLink);
    // }


    // $scope.openSource = function() {
    //     angular.element('.iframe-container').html('<div class="iframe-container"><div class="hidden-option"><img src="' + $scope.imagePath + 'pdfBlock.png" style="width:45px"></div></div>');

    //     angular.element('#pdfViewer').find('iframe').css('height', parseInt($(window).height() - 220) + 'px');
    //     angular.element('#pdfViewer').find('.modal-body').css('height', parseInt($(window).height() - 200) + 'px');
    //     // $timeout(function() {
    //     angular.element('#pdfViewer').on('shown.bs.modal', function(e) {
    //         angular.element(this).find('.iframe-container').append('<div class="hidden-option"><img src="' + $scope.imagePath + 'pdfBlock.png" style="width:45px"></div><iframe frameborder="0"  src="' + $scope.pdfUrl + '"></iframe>');
    //         //.attr('src', $scope.pdfUrl);

    //     })
    //     $scope.cover = false;
    //     // }, 1000);
    // }
    //$scope.showForward = false;

    $scope.openVideo = function (format, source) {
        const playerDesktop = new Plyr('#playerDesktop');
        angular.element('#pdfViewer').find('iframe').css('height', '100%');
        angular.element('#pdfViewer').find('.modal-body').css('height', '100%');
        $scope.testfile = 'testVideo';


        playerDesktop.source = {
            type: 'video',
            sources: [{
                src: 'https://ngssource.s3-ap-southeast-1.amazonaws.com/testpdf/testfiles/' + source,
                type: 'audio/' + format
            }]
        };

        angular.element('#pdfViewer').on('hidden.bs.modal', function () {

            playerDesktop.pause();

        });
    }

    $scope.openFile = function (source) {
        $scope.testfile = 'testFile';
        angular.element('#pdfViewer').find('iframe').css('height', parseInt($(window).height() - 220) + 'px');
        angular.element('#pdfViewer').find('.modal-body').css('height', parseInt($(window).height() - 200) + 'px');
        $scope.pdfUrl = $sce.trustAsResourceUrl('/DocViewer/DocViewer/index?url=https://ngssource.s3-ap-southeast-1.amazonaws.com/testpdf/testfiles/' + source);
    }

    $scope.openSource = function () {
        if (fileFormat == '.pdf') {
            //angular.element('.iframe-container').html('')
            //angular.element('.iframe-container').html('<div class="hidden-option"><img src="' + $scope.imagePath + 'pdfBlock.png" style="width:45px"></div>');
            angular.element('#pdfViewer').find('iframe').css('height', parseInt($(window).height() - 220) + 'px');
            angular.element('#pdfViewer').find('.modal-body').css('height', parseInt($(window).height() - 200) + 'px');
            //angular.element('#pdfViewer').on('shown.bs.modal', function(e) {
            //angular.element('#pdfViewerFrame').attr('src', $scope.pdfUrl);
            // angular.element('.iframe-container').append('<iframe id="pdfViewerFrame" frameborder="0" src="' + $scope.pdfUrl + '"></iframe>');
            //})
            //angular.element('#pdfViewer').on('shown.bs.modal', function(e) {          
            //angular.element('#pdfViewerFrame').attr('src', $scope.pdfUrl);
            // $timeout(function() {
            //     angular.element('.iframe-container').append('<iframe id="pdfViewerFrame" frameborder="0" src="'+$scope.pdfUrl+'"></iframe>');
            //     //$scope.cover = false;
            // }, 500);

            //})
        } else if (fileFormat == '.mp4') {
            const playerDesktop = new Plyr('#playerDesktop');

            playerDesktop.source = {
                type: 'video',
                sources: [{
                    src: $scope.pdfUrl,
                    type: 'video/mp4',
                    size: 720,
                }]
            };

            angular.element('#pdfViewer').on('hidden.bs.modal', function () {
                if (fileFormat == '.mp4') {
                    playerDesktop.pause();
                }
            });
            //const playerMobile = new Plyr('#playerMobile');

            // playerMobile.source = {
            //     type: 'video',
            //     sources: [
            //         {
            //             src: $scope.pdfUrl,
            //             type: 'video/mp4',
            //             size: 720,
            //         }
            //     ]
            // };

            // if ($(window).width() <= 767.98) {
            //     $scope.showForward = false;
            // } else if ($(window).width() > 767.98) {
            //     $scope.showForward = true;
            // }
        }

    }

    $scope.goToPage = function (location, from, id) {
        $state.go(location, {
            'from': from,
            'productId': id
        });
    }

    $scope.showAdvanced = function (ev) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'view/dialog/details.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true
        })
            .then(function (answer) {
                $scope.status = '';
            }, function () {
                $scope.status = '';
            });
    };

    $scope.showFile = function (ev) {
        $mdDialog.show({
            controller: fileViewerController,
            templateUrl: 'view/dialog/viewPdf.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true
        })
            .then(function (answer) {
                $scope.status = '';
            }, function () {
                $scope.status = '';
            });
    };


    // function fileViewerController($scope, $mdDialog) {

    //     if (imageCategoryCode == 'ART') {
    //         $scope.cate = 'Artwork';
    //     } else if (imageCategoryCode == 'ARC') {
    //         $scope.cate = 'Archives';
    //     } else {
    //         $scope.cate = 'Library';
    //     }

    //     $scope.imagePath = IMAGE_PATH;
    //     $scope.pdfLink = 'https://docs.google.com/viewerng/viewer?url=' + $scope.imagePath + 'asset/' + $scope.cate + '/' + filePath + '&embedded=true';
    //     $scope.dialogTitle = titletoDialog;
    //     $scope.filePath = filePath + '#toolbar=0&navpanes=0&scrollbar=0';
    //     $scope.fileFormat = fileFormat;
    //     $scope.imageCategoryCode = imageCategoryCode;
    //     //$scope.filePath = '{{imagePath}}test.pdf' + '#toolbar=0&navpanes=0&scrollbar=0';

    //     $scope.hide = function() {
    //         $mdDialog.hide();
    //     };

    //     $scope.cancel = function() {
    //         $mdDialog.cancel();
    //     };
    // }

    function DialogController($scope, $mdDialog) {
        $scope.imagePath = IMAGE_PATH;
        $scope.dialogTitle = titletoDialog;
        $scope.imageCategoryCode = imageCategoryCode;
        if (imagetoDialog) {
            $scope.dialogImage = imagetoDialog;
        }

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

    }

    $scope.addToRequest = function (id, title, artist, path, category) {
        var formData = {
            productID: id,
            sourceTitle: title,
            artist: artist,
            imagePath: path,
            category: category
        }
        angular.element('#toastBox').css({
            'right': 0
        });
        $timeout(function () {
            angular.element('#toastBox').css({
                'right': '-100%'
            });
        }, 3000);

        $scope.$parent.requestList.push(formData);
        $scope.$parent.requestLength = $scope.$parent.requestList.length;
        sessionStorage[REQUEST_STORAGE_KEY] = angular.toJson($scope.$parent.requestList);
    }

    $scope.$watch('requestLength', function (newValue, oldValue) {
        $scope.$parent.requestLength = newValue;
    });

    $scope.autoComplete = function (keyword) {
        $scope.keywords = {
            Search: keyword,
            HistorySearch: '',
            ItemCount: 7
        }
        $timeout(function () {
            ApiService.GetProductAutoComplete($scope.keywords).then(function (r) {
                angular.element('.bs_autocomplete').show();
                $scope.autoData = r.data;
                $scope.autoCompleteLength = $scope.autoData.Results.length;
            });
        }, 1000);

    }

    $scope.clickToBasicSearch = function (keyword) {
        $scope.genericSearch.Search = keyword;
        //$scope.autoComplete(keyword);
        $scope.basicSearch();
    }

    $scope.back = function () {
        $window.history.back();
    }

    $scope.open_advanceSearch = function () {
        angular.element(".search-dropdown").toggleClass("avtive");
        angular.element(".icon-advanceSearch").toggleClass("active");
    }

    $scope.artTabActive = true;
    $scope.searchTab = function (value) {
        $scope.filterValue = value;
        if (value == 'art') {
            $scope.artTabActive = true;
            $scope.libTabActive = false;
            $scope.arcTabActive = false;
        } else if (value == 'lib') {
            $scope.artTabActive = false;
            $scope.libTabActive = true;
            $scope.arcTabActive = false;
        } else {
            $scope.artTabActive = false;
            $scope.libTabActive = false;
            $scope.arcTabActive = true;
        }
    }
    $scope.focusedIndex = 0;
    $scope.handleKeyDown = function ($event) {
        var keyCode = $event.keyCode;

        if (keyCode === 40) {
            // Down
            $event.preventDefault();
            if ($scope.focusedIndex !== $scope.autoCompleteLength - 1) {
                $scope.focusedIndex++;
                $scope.genericSearch.Search = angular.element('.autocompleteResults li').eq($scope.focusedIndex).text();

            }
        } else if (keyCode === 38) {
            // Up
            $event.preventDefault();
            if ($scope.focusedIndex !== 0) {
                $scope.focusedIndex--;
                $scope.genericSearch.Search = angular.element('.autocompleteResults li').eq($scope.focusedIndex).text();

            }
        }

    };

    $scope.AdvanceSearch = {
        Search: null
    }
    $scope.basicSearch = function (val) {
        sessionStorage[FILTER_STORAGE_KEY] = 'b';
        var search_value = $scope.genericSearch.Search;
        sessionStorage[JSON_BSSEARCH_STORAGE_KEY] = search_value;
        $state.go("search", {
            'Search': search_value,
            'SearchType': 'b',
            'det': null
            // 'CategoryCode': $scope.toggleValue ? $scope.toggleValue : ''
        });
    }

    $('#DetailsPage').keypress(function (event) {

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            if (!angular.element(".search-dropdown").hasClass("avtive")) {
                sessionStorage[FILTER_STORAGE_KEY] = 'b';
                var search_value = $scope.genericSearch.Search;
                sessionStorage[JSON_BSSEARCH_STORAGE_KEY] = search_value;
                $state.go("search", {
                    'Search': search_value,
                    'SearchType': 'b',
                    'det': null
                    // 'CategoryCode': $scope.toggleValue ? $scope.toggleValue : ''
                });
            }
        }
    });

    $('#artworkDetailsPage').keypress(function (event) {

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            sessionStorage[FILTER_STORAGE_KEY] = 'b';
            var search_value = $scope.genericSearch.Search;
            sessionStorage[JSON_BSSEARCH_STORAGE_KEY] = search_value;
            $state.go("search", {
                'Search': search_value,
                'SearchType': 'b'
                // 'CategoryCode': $scope.toggleValue ? $scope.toggleValue : ''
            });
        }
    });

    $scope.AdvSearch = function () {
        sessionStorage[FILTER_STORAGE_KEY] = 'a';
        sessionStorage[RELATEDITEM_STORAGE_KEY] = false;
        angular.element(".search-dropdown").toggleClass("avtive");
        angular.element(".icon-advanceSearch").toggleClass("active");
        if ($scope.artTabActive) {
            sessionStorage[ADVSEARCHCATE_STORAGE_KEY] = 'ART';
            sessionStorage[JSON_ADVSEARCH_STORAGE_KEY] = JSON.stringify($scope.artFormData);
            $state.go("search", {
                'Search': '',
                'SearchType': 'a',
                'CategoryCode': 'ART',
                'SourceTitle': $scope.artFormData.SourceTitle,
                'SourceNo': $scope.artFormData.SourceNo,
                'Artist': $scope.artFormData.Artist,
                'Creditline': $scope.artFormData.Creditline,
                'ProductDate': $scope.artFormData.ProductDate,
                'Medium': $scope.artFormData.Medium,
                'Location': $scope.artFormData.Location,
                'det': null
            });

        } else if ($scope.libTabActive) {
            sessionStorage[ADVSEARCHCATE_STORAGE_KEY] = 'LIB';
            sessionStorage[JSON_ADVSEARCH_STORAGE_KEY] = JSON.stringify($scope.libFormData);
            $state.go("search", {
                'Search': '',
                'SearchType': 'a',
                'CategoryCode': 'LIB',
                'SourceTitle': $scope.libFormData.SourceTitle,
                'Artist': $scope.libFormData.Artist,
                'Location': $scope.libFormData.Location,
                'Language': $scope.libFormData.Language,
                'MeetingName': $scope.libFormData.MeetingName,
                'Imprint': $scope.libFormData.Imprint,
                'SeriesTitle': $scope.libFormData.SeriesTitle,
                'ISBN': $scope.libFormData.ISBN,
                'ISSN': $scope.libFormData.ISSN,
                'DeweyClass': $scope.libFormData.DeweyClass,
                'ProductDate': $scope.libFormData.ProductDate,
                'Subject': $scope.libFormData.Subject,
                'det': null
            });
        } else if ($scope.arcTabActive) {
            sessionStorage[ADVSEARCHCATE_STORAGE_KEY] = 'ARC';
            sessionStorage[JSON_ADVSEARCH_STORAGE_KEY] = JSON.stringify($scope.arcFormData);
            $state.go("search", {
                'Search': '',
                'SearchType': 'a',
                'CategoryCode': 'ARC',
                'SourceTitle': $scope.arcFormData.SourceTitle,
                'Artist': $scope.arcFormData.Artist,
                'Subject': $scope.arcFormData.Subject,
                'Language': $scope.arcFormData.Language,
                'ProductDate': $scope.arcFormData.ProductDate,
                'FondsCollection': $scope.arcFormData.FondsCollection,
                'ScopeAndContent': $scope.arcFormData.ScopeAndContent,
                'det': null
            });
        }
    }
    // $scope.showText = 'More';
    // $scope.expandDetails = function() {
    //     var $detailsTable = angular.element('.detailsTable');
    //     $detailsTable.toggleClass('active');
    //     if ($detailsTable.hasClass('active')) {
    //         $scope.showText = 'Less';
    //     } else {
    //         $scope.showText = 'More';
    //     }
    // }

    $scope.autoAdvArtistComplete = function (keyword) {
        $scope.keywords = {
            Search: keyword,
            ItemCount: 5
        }

        $timeout(function () {
            ApiService.GetArtistAutoComplete($scope.keywords).then(function (r) {
                angular.element('.bs_AdvOptionsList').show();
                $scope.autoAdvArtistData = r.data;
            });
        }, 300);

    }

    $scope.clickToAdvAuthorSearch = function (keyword) {
        angular.element('.bs_AdvOptionsList').hide();
        $scope.libFormData.Artist = keyword;
    }

    $scope.autoAdvAuthorComplete = function (keyword) {
        $scope.keywords = {
            Search: keyword,
            ItemCount: 5
        }

        $timeout(function () {
            ApiService.GetArtistAutoComplete($scope.keywords).then(function (r) {
                angular.element('.bs_AdvOptionsList').show();
                $scope.autoAdvAuthorData = r.data;
            });
        }, 300);

    }

    $scope.clickToAdvArtistSearch = function (keyword) {
        angular.element('.bs_AdvOptionsList').hide();
        $scope.artFormData.Artist = keyword;
    }

    $scope.collectionSelection = function () {
        $timeout(function () {
            angular.element('.bs_AdvOptionsCollectionList').show();
        }, 300);
    }

    $scope.clickToCollectionField = function (val) {
        $scope.libFormData.Collection = val;
        angular.element('.bs_AdvOptionsCollectionList').hide();
    }

    $scope.back = function () {
        $window.history.back();
    }

}]);

app.controller('cartCtrl', ['$scope', '$state', '$location', '$http', '$mdDialog', function ($scope, $state, $location, $http, $mdDialog) {
    $scope.$parent.location = 'cart';
    $scope.$parent.hasOptionBar = false;
    $scope.CartEmpty = true;

    if(DOWNTIME){
        $state.go('downtime');
    }

    $scope.myRequest = sessionStorage[REQUEST_STORAGE_KEY] ? JSON.parse(sessionStorage[REQUEST_STORAGE_KEY]) : '';
    $scope.$parent.requestLength = $scope.myRequest ? $scope.myRequest.length : 0;
    //var i = $scope.myRequest.length;
    $scope.deleteVal = 0;
    $scope.removeItem = function (val) {
        var index = -1;
        var filteredObj = $scope.myRequest.find(function (item, i) {
            if (item.productID === val) {
                index = i;
                $scope.deleteVal = i;
            }
        });

        $scope.myRequest.splice($scope.deleteVal, 1);

        $scope.$parent.requestList.length = 0;
        $scope.$parent.requestList.push($scope.myRequest);
        sessionStorage[REQUEST_STORAGE_KEY] = angular.toJson($scope.$parent.requestList);
        $scope.$parent.requestLength = $scope.myRequest.length;

    }
    $scope.$watch('requestLength', function (newValue, oldValue) {
        $scope.$parent.requestLength = newValue;

        if (newValue == 0) {
            sessionStorage.clear();
        }
    });

    $scope.bookmark = function (ev) {
        $mdDialog.show({
            controller: EmailRequestController,
            templateUrl: 'view/dialog/bookmark.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true
        })
            .then(function (answer) {
                $scope.status = '';
            }, function () {
                $scope.status = '';
            });
    }

    function EmailRequestController($scope, $mdDialog) {

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.sendRequest = function () {
            angular.element('#emailRequestForm').hide();
            angular.element('.requestSuccess').show(100);
        }

    }

}]);

app.controller('aboutCtrl', ['$scope', '$state', '$location', '$http', '$window', function ($scope, $state, $location, $http, $window) {
    $scope.$parent.location = 'about';
    $scope.$parent.homeTop = true;
    $scope.$parent.hasOptionBar = false;

    if(DOWNTIME){
        $state.go('downtime');
    }

    $scope.back = function () {
        $window.history.back();
    }

}]);

app.controller('termConditionCtrl', ['$scope', '$state', '$location', '$http', '$window', function ($scope, $state, $location, $http, $window) {
    $scope.$parent.location = 'termCondition';
    $scope.$parent.homeTop = true;
    $scope.$parent.hasOptionBar = false;

    if(DOWNTIME){
        $state.go('downtime');
    }

    $scope.back = function () {
        $window.history.back();
    }

    $scope.isCheckTerm = function () {
        sessionStorage[JSON_ISCHECKTERM_STORAGE_KEY] = 'true';
    }

}]);

app.controller('personalParticularCtrl', ['$scope', '$location', '$http', '$mdDialog', '$compile', 'ApiService', '$state', '$window', function ($scope, $location, $http, $mdDialog, $compile, ApiService, $state, $window) {
    if(DOWNTIME){
        $state.go('downtime');
    }

    var term = sessionStorage[JSON_ISCHECKTERM_STORAGE_KEY];
    $scope.checkTermIsCheck = function () {
        if (!term) {
            $state.go('termcondition');
        }
    }

    $scope.validateInput = /^[A-Za-z0-9_@.#$=!%^)(\]:\*\?\~`,'\\|\[&\+-\- ]*$/;
    $scope.testValue = /^["<>{}/;]*$/

    $scope.count = 1;
    $scope.$parent.homeTop = true;
    $scope.$parent.hasOptionBar = false;
    $scope.$parent.location = 'personal-particular';
    $scope.formData = {
        typeOfInfo: "",
    }

    $scope.categoryItemSelect1 = 'art';
    $scope.back = function () {
        $window.history.back();
    }

    $scope.checkValidate = function (value) {
        if ($scope.testValue.test(value)) {
            return true;
        }
        return false;
    }

    $scope.changeInfo = function () {

        for (var i = 1; i <= $scope.count; i++) {
            $scope.formData[i] = {
                Title: "",
                Creator: "",
                PeriodOfUse: "",
                Territory: "",
                Format: "",
                UseOfImage: "",
                Language: "",

            }
        }
        $scope.count = 1;
    }

    $scope.formDataItems = [{
        CategoryCode: "ART",
        Artist: "",
        Title: "",
        AccessionNo: ""
    }];

    $scope.removeItem = function (itemIndex) {
        $scope.formDataItems.splice(itemIndex, 1);
    }
    $scope.addMore = function (data, index) {

        var itemToClone = angular.copy(data);
        $scope.formDataItems.push(itemToClone);
        $scope.formDataItems[index].Artist = '';
        $scope.formDataItems[index].Title = '';
        $scope.formDataItems[index].AccessionNo = '';
    }
    $scope.requestSubmit = function () {

        $scope.loading = true;

        $scope.requestedData = {
            RequestorName: $scope.formData.RequestorName,
            RequestorEmail: $scope.formData.RequestorEmail,
            RequestorTitle: $scope.formData.RequestorTitle,
            OrganisationName: $scope.formData.OrganisationName,
            Address: $scope.formData.Address,
            ProjectType: $scope.formData.typeOfInfo,
            EducationalOption: $scope.formData.EducationalOption,
            Description: $scope.formData.Description,
            Creator: $scope.formData.Creator,
            PeriodOfUse: $scope.formData.PeriodOfUse,
            Format: $scope.formData.Format,
            Language: $scope.formData.Language,
            UseOfImage: $scope.formData.UseOfImage,
            Publisher: $scope.formData.Publisher,
            PrintRun: $scope.formData.PrintRun,
            RequestorRemark: $scope.formData.RequestorRemark,
            RequestItems: $scope.formDataItems,
            SurveyRotunda: $scope.formData.surveyRotunda,
            SurveyRotundaOther: $scope.formData.surveyRotundaOther,
        }

        ApiService.CreateRequest($scope.requestedData)
            .then(function (r) {

                if (r.statusText === 'OK') {

                    $scope.loading = false;
                    $state.go('success', {
                        id: r.data.Request.RequestId
                    });

                } else {
                    $window.alert('Form Submission Failed');
                }

            });
    }
}]);

app.controller('faqCtrl', ['$scope', '$state', '$location', '$http', '$window', 'ApiService', '$timeout', function ($scope, $state, $location, $http, $window, ApiService, $timeout) {
    $scope.$parent.location = 'faq';
    $scope.$parent.homeTop = true;
    $scope.$parent.hasOptionBar = false;

    if(DOWNTIME){
        $state.go('downtime');
    }

    $scope.back = function () {
        $window.history.back();
    }

    ApiService.GetFAQs()
        .then(function (r) {
            if (r.statusText === 'OK') {
                $scope.faqs = r.data.FAQs;

                $timeout(function () {
                    $('.accordianClick').click(function (e) {
                        e.preventDefault();
                        var showTable = $(this).attr("id");

                        if ($('.' + showTable).is(":visible")) {
                            $('.' + showTable).toggle(100);
                            $(this).find('.icons8').css({
                                "transform": "rotate(0deg)"
                            });
                        } else {
                            $('.' + showTable).toggle(100);
                            $(this).find('.icons8').css({
                                "transform": "rotate(90deg)"
                            });
                        }
                        //$(".image2").attr("src","image1.jpg");
                    });
                }, 300);
            } else {

            }

        });

    $(document).ready(function () {


    });
}]);

app.controller('backToTopCtrl', ['$scope', '$state', '$location', '$http', '$window', function ($scope, $state, $location, $http, $window) {
    $(window).scroll(function () {
        var scrollHeight = $(window).scrollTop();
        if (scrollHeight > 1000) {
            angular.element('#backtoTop').css({
                'opacity': '1'
            });
        } else {
            angular.element('#backtoTop').css({
                'opacity': '0'
            });
        }
    });

    $scope.backtotop = function () {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

}]);

app.controller('successCtrl', ['$scope', '$state', '$location', '$http', 'ApiService', '$stateParams', '$window', function ($scope, $state, $location, $http, ApiService, $stateParams, $window) {
    $scope.id = $stateParams.id;
    $scope.$parent.hasOptionBar = false;
    $scope.surveyList = {
        items: [{
            name: 'Gallery visit',
            checked: false
        }, {
            name: 'Gallery website',
            checked: false
        }, {
            name: 'Internet search',
            checked: false
        }, {
            name: 'Other library/archive',
            checked: false
        }, {
            name: 'Personal recommendation',
            checked: false
        }, {
            name: 'University/school',
            checked: false
        }, {
            name: 'Other',
            checked: false
        }]
    }

    if(DOWNTIME){
        $state.go('downtime');
    }

    $scope.thankyouMessage = function () {
        $scope.formData = {
            RequestId: $scope.id,
            Survey: $scope.selectedSurvey
        }

        ApiService.CreateSurvey($scope.formData)
            .then(function (r) {
                if (r.statusText === 'OK') {
                    angular.element('#surveyForm').html('<p class="text-bold text-success">Thanks for submitting your survey!</p>');
                } else {
                    $window.alert('Survey Submission Failed');
                }

            });

    }

    $scope.selectedSurvey = [];
    $scope.pickSurvey = function (item) {
        var removeSurveyList = [];

        if (item.checked === true) {
            $scope.selectedSurvey.push(item.name);

        } else {
            removeSurveyList.push(item.name);
            var i = $scope.selectedSurvey.length;
            while (i--) {
                if (removeSurveyList.indexOf($scope.selectedSurvey[i]) != -1) {
                    $scope.selectedSurvey.splice(i, 1);
                }
            }
        }

    }


}]);

app.controller('downtimeCtrl', ['$scope', '$state', '$location', '$http', '$window', function ($scope, $state, $location, $http, $window){
    if(DOWNTIME){
        let getHeader = document.getElementById('navbar');
        if(getHeader){
            document.getElementById('navbar').style.display = 'none';
        }
    }
    else {
        $state.go('home');
    }
}]);