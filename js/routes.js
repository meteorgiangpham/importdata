app
.config(["$stateProvider", "$locationProvider", "$urlRouterProvider", function($stateProvider, $locationProvider, $urlRouterProvider) {
    'use strict';

    $urlRouterProvider.otherwise("/");

    $locationProvider.html5Mode({
        enabled: false,
        requireBase: false
    }).hashPrefix('');

    $stateProvider 
        .state('home', { 
            url : '/', 
            templateUrl: 'view/home.html',
            controller  : 'homeCtrl',
            reloadOnSearch: true,
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                // you can lazy load files for an existing module
                       return $ocLazyLoad.load('js/app.js');
              }]
            }
        })
        .state('about', { 
            url : '/about', 
             templateUrl: 'view/about.html',
             controller  : 'aboutCtrl'
        })
        .state('cart', { 
            url : '/cart', 
             templateUrl: 'view/cart.html',
             controller  : 'cartCtrl'
        })
        .state('search', {
            url : '/search?Search&SearchType&det&CategoryCode&SourceTitle&Artist&ArtistDateOfBirth&ArtistDateOfDeath&ProductDate&Imprint&Creditline&Medium&Location&Subject&Language&MeetingName&SeriesTitle&ISBN&ISSN&DeweyClass&Collection&OnDisplay&MaterialType&AccessLevel&StorageLocation&FilterLanguage&FilterArtist&FilterLocation&FilterSubject&ItemAvailability&ImageAvailability&FilterLanguageOperator&FilterMedium&FondsCollection&ScopeAndContent&relatedSearch&ProductId', 
            templateUrl : 'view/search.html',
            controller  : 'searchCtrl',
            reloadOnSearch: false,
             resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                // you can lazy load files for an existing module
                       return $ocLazyLoad.load('js/app.js');
              }]
            }
        })
        .state('details', {
          url : '/details/:from/:productId?BRN', 
          templateUrl : 'view/details.html',
          controller  : 'detailsCtrl'
        })
        // .state('artwork_details', {
        //   url : '/artwork_details/:productId/?from',
        //   templateUrl : 'view/artwork_details.html',
        //   controller  : 'detailsCtrl'
        // })
        .state('termcondition', {
          url : '/termCondition',
          templateUrl : 'view/termCondition.html',
          controller  : 'termConditionCtrl'
        })
        .state('personal-particular', {
          url : '/personal-particular',
          templateUrl : 'view/personal_particular.html',
          controller  : 'personalParticularCtrl'
        })
        .state('success', {
          url : '/success/:id',
          templateUrl : 'view/success.html',
          controller  : 'successCtrl'
        })
        .state('faq', {
          url : '/faq',
          templateUrl : 'view/faq.html',
          controller  : 'faqCtrl'
        })
        .state('downtime', {
          url : '/downtime',
          templateUrl : 'view/downtime.html',
          controller  : 'downtimeCtrl'
        })
      
}]).config(function($provide) {
    $provide.decorator('$state', function($delegate, $stateParams) {
        $delegate.forceReload = function() {
            return $delegate.go($delegate.current, $stateParams, {
                reload: false,
                inherit: false,
                notify: false
            });
        };
        return $delegate;
    });
});