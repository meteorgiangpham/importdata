app
    .factory("DataService", [function() {

        var _data = {};

        return {
            get: function(key) {
                return _data[key];
            },
            set: function(key, value) {
                _data[key] = value;
            }
        }
    }])
    .factory("ApiService", ["$http", function($http) {
        var post = "POST";
        var get = "GET";
        var base = "https://stg-collections.nationalgallery.sg/PublicApi/";
        var config = {
            getproduct: base + "ProductApi/getproduct",
            GetProductFilter: base + "ProductApi/GetProductFilter",
            CreateRequest: base + "Requestapi/CreateRequest",
            GetProductAutoComplete: base + "ProductApi/GetProductAutoComplete",
            GetArtistAutoComplete: base + "ProductApi/GetArtistAutoComplete",
            CreateSurvey: base + "Requestapi/CreateSurvey",
            GetProductCount: base + "ProductApi/GetProductCount",
            checkmode: base + "sessionapi/checkmode",
            SetMode: base + "SessionApi/SetMode",
            GetFAQs: base + "FAQApi/GetFAQs",
            GetFile: base + "ProductApi/GetFile",
            GetOrderStatus: base + " RequestApi/GetOrderStatus",
            trackItemClick: base + " ProductApi/TrackItemClick"
        };
        return {
            getproduct: function(formData) {
                return $http({
                    "url": config.getproduct,
                    "data": formData,
                    "method": post,
                    "headers": {
                        'Content-Type': 'application/json; charset=utf-8',
                        'access-control-allow-origin': '*'
                    },
                    "cache": false,
                    "dataType": 'json',
                });
            },
            GetProductFilter: function(formData) {
                return $http({
                    "url": config.GetProductFilter,
                    "data": formData,
                    "method": post,
                    "headers": {
                        'Content-Type': 'application/json; charset=utf-8',
                        'access-control-allow-origin': '*'
                    }
                });
            },
            CreateRequest: function(formData) {
                return $http({
                    "url": config.CreateRequest,
                    "data": formData,
                    "method": post,
                    "headers": {
                        'Content-Type': 'application/json; charset=utf-8',
                        'access-control-allow-origin': '*'
                    }
                });
            },
            GetProductAutoComplete: function(formData) {
                return $http({
                    "url": config.GetProductAutoComplete,
                    "data": formData,
                    "method": post,
                    "headers": {
                        'Content-Type': 'application/json; charset=utf-8',
                        'access-control-allow-origin': '*'
                    }
                });
            },
            CreateSurvey: function(formData) {
                return $http({
                    "url": config.CreateSurvey,
                    "data": formData,
                    "method": post,
                    "headers": {
                        'Content-Type': 'application/json; charset=utf-8',
                        'access-control-allow-origin': '*'
                    }
                });
            },
            GetArtistAutoComplete: function(formData) {
                return $http({
                    "url": config.GetArtistAutoComplete,
                    "data": formData,
                    "method": post,
                    "headers": {
                        'Content-Type': 'application/json; charset=utf-8',
                        'access-control-allow-origin': '*'
                    }
                });
            },
            GetProductCount: function(formData) {
                return $http({
                    "url": config.GetProductCount,
                    "data": formData,
                    "method": post,
                    "headers": {
                        'Content-Type': 'application/json; charset=utf-8',
                        'access-control-allow-origin': '*'
                    }
                });
            },
            checkmode: function(formData) {
                return $http({
                    "url": config.checkmode,
                    "data": formData,
                    "method": post,
                    "headers": {
                        'Content-Type': 'application/json; charset=utf-8',
                        'access-control-allow-origin': '*'
                    }
                });
            },
            SetMode: function(formData) {
                return $http({
                    "url": config.SetMode,
                    "data": formData,
                    "method": post,
                    "headers": {
                        'Content-Type': 'application/json; charset=utf-8',
                        'access-control-allow-origin': '*'
                    }
                });
            },
            GetFAQs: function() {
                return $http({
                    "url": config.GetFAQs,
                    "method": post,
                    "headers": {
                        'Content-Type': 'application/json; charset=utf-8',
                        'access-control-allow-origin': '*'
                    }
                });
            },
            GetFile: function(formData) {
                return $http({
                    "url": config.GetFile,
                    "params": formData,
                    "method": post,
                    "headers": {
                        'Content-Type': 'application/json; charset=utf-8',
                        'access-control-allow-origin': '*'
                    }
                });
            },
            GetOrderStatus: function() {
                return $http({
                    "url": config.GetOrderStatus,
                    "method": post,
                    "headers": {
                        'Content-Type': 'application/json; charset=utf-8',
                        'access-control-allow-origin': '*'
                    }
                });
            },
            TrackItemClick: function(formData) {
                return $http({
                    "url": config.trackItemClick,
                    "data": formData,
                    "method": post,
                    "headers": {
                        'Content-Type': 'application/json; charset=utf-8',
                        'access-control-allow-origin': '*'
                    }
                });
            },
        }
    }])
    .filter("durationFormat", [function() {
        return function(input) {
            var output = "";
            input = input.split(":");

            while (input.length > 0) {
                var cnt = input.length;
                var tmp = parseInt(input.shift());

                if (tmp != 0) {
                    output += " " + tmp;
                    switch (cnt) {
                        case 3:
                            output += " hr" + (tmp > 1 ? "s" : "");
                            break;
                        case 2:
                            output += " min" + (tmp > 1 ? "s" : "");
                            break;
                        case 1:
                            output += " sec" + (tmp > 1 ? "s" : "");
                            break;
                    }
                }
            }
            return $.trim(output) || "0 sec";
        }
    }])
    .filter("timeFormat", [function() {
        return function(input) {
            var output = "";
            var first = true;
            input = input.split(":");

            while (input.length > 0) {
                var cnt = input.length;
                var tmp = input.shift();

                if (parseInt(tmp) != 0 || !first || input.length <= 1) {
                    first = false;
                    output = output + (output ? ":" : "") + tmp;
                }

            }
            return $.trim(output) || "00:00";
        }
    }]).filter('secondsToDateTime', function() {
        return function(seconds) {
            var d = new Date(0, 0, 0, 0, 0, 0, 0);
            d.setSeconds(seconds);
            return d;
        };
    })
    .filter("currencyFormat", [function() {
        return function(input) {
            var val = Math.round(Number(input) * 100) / 100;
            //var parts = val.toString().split(".");
            //var num = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "." + (+parts[1] ? parts[1] + (+parts[1] > 9 ? "0" : "") : "00");

            return "SGD " + val.toFixed(2);
        }
    }])
    .filter('toMinSec', function() {
        return function(input) {
            var minutes = parseInt(input / 60, 10);
            var seconds = input % 60;

            return minutes + ' mins' + (seconds ? ' ' + seconds + ' sec' : '');
        }
    })
    .filter('toHrMinSec', function() {
        return function(input) {
            var hour = parseInt(input / 60 / 60, 10);
            var minutes = parseInt(input / 60, 10);
            var seconds = input % 60;

            return (hour ? hour + ':' : '00:') + (minutes ? minutes + ':' : '00:') + (seconds ? seconds + ':' : '00');
        }
    })
    .filter("MaskFunc", [function() {
        return function(input) {
            var maskid = "";
            var myemailId = input;
            var prefix = myemailId.substring(0, myemailId.lastIndexOf("@"));
            var postfix = myemailId.substring(myemailId.lastIndexOf("@"));

            for (var i = 0; i < prefix.length; i++) {
                if (i == 0 || i == prefix.length - 1) { ////////
                    maskid = maskid + prefix[i].toString();
                } else {
                    maskid = maskid + "*";
                }
            }
            maskid = maskid + postfix;
            return maskid;
        }


    }]).filter('cut', function() {
        return function(value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace !== -1) {
                    //Also remove . and , so its gives a cleaner result.
                    if (value.charAt(lastspace - 1) === '.' || value.charAt(lastspace - 1) === ',') {
                        lastspace = lastspace - 1;
                    }
                    value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    })

.service('anchorSmoothScroll', function() {

    this.scrollTo = function(eID) {

        // This scrolling function 
        // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript

        var startY = currentYPosition();
        var stopY = elmYPosition(eID);
        var distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            scrollTo(0, stopY);
            return;
        }
        var speed = Math.round(distance / 100);
        if (speed >= 20) speed = 20;
        var step = Math.round(distance / 25);
        var leapY = stopY > startY ? startY + step : startY - step;
        var timer = 0;
        if (stopY > startY) {
            for (var i = startY; i < stopY; i += step) {
                setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                leapY += step;
                if (leapY > stopY) leapY = stopY;
                timer++;
            }
            return;
        }
        for (var i = startY; i > stopY; i -= step) {
            setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
            leapY -= step;
            if (leapY < stopY) leapY = stopY;
            timer++;
        }

        function currentYPosition() {
            // Firefox, Chrome, Opera, Safari
            if (self.pageYOffset) return self.pageYOffset;
            // Internet Explorer 6 - standards mode
            if (document.documentElement && document.documentElement.scrollTop)
                return document.documentElement.scrollTop;
            // Internet Explorer 6, 7 and 8
            if (document.body.scrollTop) return document.body.scrollTop;
            return 0;
        }

        function elmYPosition(eID) {
            var elm = document.getElementById(eID);
            var y = elm.offsetTop;
            var node = elm;
            while (node.offsetParent && node.offsetParent != document.body) {
                node = node.offsetParent;
                y += node.offsetTop;
            }
            return y;
        }

    };

}).filter('trustThisUrl', ["$sce", function($sce) {
    return function(val) {
        return $sce.trustAsResourceUrl(val);
    };
}]);
// .factory('Reddit', function($http, ApiService) {
//   var Reddit = function() {
//     this.items = [];
//     this.busy = false;
//     this.after = '';
//   };

//   Reddit.prototype.nextPage = function() {
//     if (this.busy) return;
//     this.busy = true;
//     ApiService.getproduct()
//     .then(function (r) {
//         var items = r.data.Products;
//         for (var i = 0; i < items.length; i++) {
//         this.items.push(items[i]);
//       }
//         this.after = "t3_" + this.items[this.items.length - 1].ProductId;
//       this.busy = false;
//     }.bind(this));

//     // var url = "https://api.reddit.com/hot?after=" + this.after + "&jsonp=JSON_CALLBACK";
//     // $http.jsonp(url).success(function(data) {
//     //   var items = data.data.children;
//     //   for (var i = 0; i < items.length; i++) {
//     //     this.items.push(items[i].data);
//     //   }
//     //   this.after = "t3_" + this.items[this.items.length - 1].id;
//     //   this.busy = false;
//     // }.bind(this));
//   };

//   return Reddit;
// });