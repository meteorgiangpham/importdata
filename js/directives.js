app.directive('headerComponent', function () {
    return {
        templateUrl: 'view/components/header.html'
    };
}).directive('advanceSearch', function () {
    return {
        templateUrl: 'view/components/advance-search.html'
    };
}).directive('searchFilter', function () {
    return {
        templateUrl: 'view/components/search-filter.html'
    };
}).directive('searchComponent', function () {
    return {
        templateUrl: 'view/components/search.html'
    };
}).directive('scrollTrigger', function($window) {
    return {
        link : function(scope, element, attrs) {
            var offset = parseInt(attrs.threshold) || 0;
            var e = angular.element(element[0]);
            var doc = angular.element($window);
            angular.element($window).bind('scroll', function() {
                if (doc.scrollTop() + $window.innerHeight + offset > e.offset().top) {
                    scope.$apply(attrs.scrollTrigger);
                }
            });
        }
    };
}).directive('backToTop', function () {
    return {
        templateUrl: 'view/components/backToTop.html',
        controller: "backToTopCtrl"
    };
}).directive('onErrorSrc', function() {
    return {
        link: function(scope, element, attrs) {
          element.bind('error', function() {
            if (attrs.src != attrs.onErrorSrc) {
              attrs.$set('src', attrs.onErrorSrc);
            }
          });
        }
    }
}).directive('refreshable', [function () {
    return {
        restrict: 'A',
        scope: {
            refresh: "=refreshable"
        },
        link: function (scope, element, attr) {
            var refreshMe = function () {
                element.attr('src', element.attr('src'));
            };

            scope.$watch('refresh', function (newVal, oldVal) {
                if (scope.refresh) {
                    scope.refresh = false;
                    refreshMe();
                }
            });
        }
    };
}])